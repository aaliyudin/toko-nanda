<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRequestOrderDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('request_order_details', function (Blueprint $table) {
          $table->increments('id');
          $table->integer('request_order_id');
          $table->integer('product_id');
          $table->integer('qty');
          $table->float('discount_product',11,2);
          $table->float('discount_brand',11,2)->default(0.00);
          $table->float('total_price',11,2);
          $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('request_order_details');
    }
}
