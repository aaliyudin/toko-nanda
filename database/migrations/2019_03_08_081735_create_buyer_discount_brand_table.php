<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBuyerDiscountBrandTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('buyer_discount_brand', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('buyer_id');
            $table->integer('brand_id');
            $table->float('discount_1',11,2);
            $table->float('discount_2',11,2);
            $table->float('discount_3',11,2);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('buyer_discount_brand');
    }
}
