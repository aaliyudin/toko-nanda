<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('/supplier');
});

Route::group(['prefix'=>'supplier'],function(){
  Route::get('/', 'SupplierController@index')->name('index_supplier');
  Route::get('/add', 'SupplierController@add')->name('add_supplier');
  Route::get('/edit', 'SupplierController@edit')->name('edit_supplier');
  Route::post('/create', 'SupplierController@create')->name('create_supplier');
  Route::post('/update', 'SupplierController@update')->name('update_supplier');
  Route::get('/delete', 'SupplierController@delete');
});

Route::group(['prefix'=>'buyer'],function(){
  Route::get('/', 'BuyerController@index')->name('index_buyer');
  Route::get('/add', 'BuyerController@add')->name('add_buyer');
  Route::get('/edit', 'BuyerController@edit')->name('edit_buyer');
  Route::post('/create', 'BuyerController@create')->name('create_buyer');
  Route::post('/update', 'BuyerController@update')->name('update_buyer');
  Route::get('/delete', 'BuyerController@delete');
});

Route::group(['prefix'=>'product_unit'],function(){
  Route::get('/', 'ProductUnitController@index')->name('index_product_unit');
  Route::get('/add', 'ProductUnitController@add')->name('add_product_unit');
  Route::get('/edit', 'ProductUnitController@edit')->name('edit_product_unit');
  Route::post('/create', 'ProductUnitController@create')->name('create_product_unit');
  Route::post('/update', 'ProductUnitController@update')->name('update_product_unit');
  Route::get('/delete', 'ProductUnitController@delete');
});

Route::group(['prefix'=>'order_status'],function(){
  Route::get('/', 'OrderStatusController@index')->name('index_order_status');
  Route::get('/add', 'OrderStatusController@add')->name('add_order_status');
  Route::get('/edit', 'OrderStatusController@edit')->name('edit_order_status');
  Route::post('/create', 'OrderStatusController@create')->name('create_order_status');
  Route::post('/update', 'OrderStatusController@update')->name('update_order_status');
  Route::get('/delete', 'OrderStatusController@delete');
});

Route::group(['prefix'=>'request_order_status'],function(){
  Route::get('/', 'RequestOrderStatusController@index')->name('index_request_order_status');
  Route::get('/add', 'RequestOrderStatusController@add')->name('add_request_order_status');
  Route::get('/edit', 'RequestOrderStatusController@edit')->name('edit_request_order_status');
  Route::post('/create', 'RequestOrderStatusController@create')->name('create_request_order_status');
  Route::post('/update', 'RequestOrderStatusController@update')->name('update_request_order_status');
  Route::get('/delete', 'RequestOrderStatusController@delete');
});

Route::group(['prefix'=>'brand'],function(){
  Route::get('/', 'BrandController@index')->name('index_brand');
  Route::get('/add', 'BrandController@add')->name('add_brand');
  Route::get('/edit', 'BrandController@edit')->name('edit_brand');
  Route::post('/create', 'BrandController@create')->name('create_brand');
  Route::post('/update', 'BrandController@update')->name('update_brand');
  Route::get('/delete', 'BrandController@delete');
});

Route::group(['prefix'=>'product'],function(){
  Route::get('/', 'ProductController@index')->name('index_product');
  Route::get('/add', 'ProductController@add')->name('add_product');
  Route::get('/edit', 'ProductController@edit')->name('edit_product');
  Route::post('/create', 'ProductController@create')->name('create_product');
  Route::post('/update', 'ProductController@update')->name('update_product');
  Route::get('/delete', 'ProductController@delete');
});

Route::group(['prefix'=>'request_order'],function(){
  Route::get('/', 'RequestOrderController@index')->name('index_request_order');
  Route::get('/add', 'RequestOrderController@add')->name('add_request_order');
  Route::get('/edit', 'RequestOrderController@edit')->name('edit_request_order');
  Route::post('/create', 'RequestOrderController@create')->name('create_request_order');
  Route::post('/update', 'RequestOrderController@update')->name('update_request_order');
  Route::get('/delete', 'RequestOrderController@delete')->name('delete_request_order');
});

Route::group(['prefix'=>'order'],function(){
  Route::get('/', 'OrderController@index')->name('index_order');
  Route::get('/add', 'OrderController@add')->name('add_order');
  Route::get('/edit', 'OrderController@edit')->name('edit_order');
  Route::post('/create', 'OrderController@create')->name('create_order');
  Route::post('/update', 'OrderController@update')->name('update_order');
  Route::get('/delete', 'OrderController@delete');
});

Route::group(['prefix'=>'ajax'],function(){
  Route::post('/product_by_supplier', 'ajaxController@prodcutBySupplier')->name('product_by_supplier');
  Route::post('/product_get_price', 'ajaxController@prodcutGetPrice')->name('product_get_price');
  Route::post('/request_order_by_supplier', 'ajaxController@requestOrderBySupplier')->name('request_order_by_supplier');
  Route::post('/get_product_total_by_request_order', 'ajaxController@getProductTotalByRequestOrder')->name('get_product_total_by_request_order');
  Route::post('/show_order_detail', 'ajaxController@showOrderDetail')->name('show_order_detail');
  Route::post('/show_request_order_detail', 'ajaxController@showRequestOrderDetail')->name('show_request_order_detail');
  Route::get('/prnit_struk_buyer', 'ajaxController@printStrukBuyer')->name('prnit_struk_buyer');
  Route::get('/prnit_struk', 'ajaxController@printStruk')->name('prnit_struk');
  Route::get('/all_product', 'ajaxController@allProduct')->name('all_product');
  Route::get('/discount_by_buyer', 'ajaxController@discountByBuyer')->name('discount_by_buyer');
});
Route::get('/test', 'OrderController@test')->name('test');
