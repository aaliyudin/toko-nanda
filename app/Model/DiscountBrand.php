<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class DiscountBrand extends Model
{
  protected $guarded = ['id'];
  protected $table = 'buyer_discount_brand';

  public function buyer()
  {
      return $this->belongsTo('App\Model\Buyer', 'buyer_id');
  }
}
