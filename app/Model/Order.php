<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
  protected $guarded = ['id'];

  public function detail()
  {
      return $this->hasMany('App\Model\OrderDetail','order_id','id');
  }
  public function status(){
      return $this->belongsTo('App\Model\Supplier', 'status_id','id');
  }
  public function supplier(){
      return $this->belongsTo('App\Model\Supplier', 'supplier_id','id');
  }
}
