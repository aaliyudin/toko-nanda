<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class RequestOrder extends Model
{
  protected $guarded = ['id'];

  public function detail()
  {
      return $this->hasMany('App\Model\RequestOrderDetail','request_order_id','id');
  }
  public function supplier()
  {
      return $this->belongsTo('App\Model\Supplier', 'supplier_id');
  }
  public function buyer()
  {
      return $this->belongsTo('App\Model\Buyer', 'buyer_id');
  }
}
