<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Brand extends Model
{
  protected $guarded = ['id'];

  public function product(){
    return $this->hasMany('App\Models\Product\Product');
  }
}
