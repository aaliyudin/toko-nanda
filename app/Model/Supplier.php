<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Supplier extends Model
{
  protected $guarded = ['id'];

  public function product()
  {
      return $this->hasMany('App\Model\Product');
  }
  public function requestOrder()
  {
    return $this->hasMany('App\Model\RequestOrder');
  }
  public function order()
  {
    return $this->hasMany('App\Model\Order');
  }
}
