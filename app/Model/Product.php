<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
  protected $guarded = ['id'];

  public function supplier(){
      return $this->belongsTo('App\Model\Supplier', 'supplier_id','id');
  }
  public function unit(){
      return $this->belongsTo('App\Model\ProductUnit', 'unit_id');
  }
  public function brand(){
      return $this->belongsTo('App\Model\Brand', 'brand_id');
  }
  public function requestOrderDetail(){
      return $this->hasMany('App\Model\RequestOrderDetail');
  }
  public function orderDetail(){
      return $this->hasMany('App\Model\OrderDetail');
  }
}
