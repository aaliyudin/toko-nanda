<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class StatusOrder extends Model
{
    protected $guarded = ['id'];

    public function order()
    {
      return $this->hasMany('App\Model\Order');
    }
}
