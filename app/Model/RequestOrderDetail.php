<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class RequestOrderDetail extends Model
{
  protected $guarded = ['id'];

  public function requestOrder()
  {
      return $this->belongsTo('App\Model\RequestOrder');
  }
  public function product()
  {
      return $this->belongsTo('App\Model\Product', 'product_id');
  }
}
