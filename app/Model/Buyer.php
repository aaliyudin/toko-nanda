<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Buyer extends Model
{
  protected $guarded = ['id'];

  public function requestOrder()
  {
    return $this->hasMany('App\Model\RequestOrder');
  }
  public function discountBrand()
  {
    return $this->hasMany('App\Model\DiscountBrand');
  }
}
