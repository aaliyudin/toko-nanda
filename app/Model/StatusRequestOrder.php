<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class StatusRequestOrder extends Model
{
  protected $guarded = ['id'];

  public function requestOrder()
  {
    return $this->hasMany('App\Model\RequestOrder');
  }
}
