<?php
namespace App\Http\Controllers;

use Response;
use App\Http\Libraries\Assets;
use Illuminate\Http\Request;
use App\Model\Supplier;

class SupplierController extends Controller {

  public function index(Request $request)
  {
    $suppliers = Supplier::all();

    $data = [
      'suppliers' => $suppliers
    ];
    return view('supplier.index')->with($data);
  }

  public function add(Request $request)
  {
    return view('supplier.add');
  }

  public function edit(Request $request)
  {
    $supplierId = $_GET['supplier_id'];
    $supplierUpdate = Supplier::find($supplierId);

    $data = [
      'supplier' => $supplierUpdate
    ];
    return view('supplier.edit')->with($data);
  }

  public function create(Request $request)
  {
    $data['name'] = $request->input('name');
    $data['phone'] = $request->input('phone');
    $data['address'] = $request->input('address');
    $data['company'] = $request->input('company');

    $supplierCreate = Supplier::create($data);
    return redirect()->back()->with('message', 'Berhasil Menambahkan Supplier');
  }

  public function update(Request $request)
  {
    $supplierId = $request->input('supplier_id');
    $supplierUpdate = Supplier::find($supplierId);

    $supplierUpdate->name = $request->input('name');
    $supplierUpdate->phone = $request->input('phone');
    $supplierUpdate->address = $request->input('address');
    $supplierUpdate->company = $request->input('company');
    $supplierUpdate->save();

    return redirect()->back()->with('message', 'Berhasil Edit Supplier');
  }

  public function delete(Request $request)
  {
    $supplierId = $request->input('supplier_id');
    $supplierDelete = Supplier::find($supplierId);

    $supplierDelete->delete();

    return redirect()->back()->with('message', 'Berhasil Menghapus Supplier');
  }

}
