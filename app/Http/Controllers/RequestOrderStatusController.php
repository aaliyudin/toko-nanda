<?php

namespace App\Http\Controllers;

use Response;
use App\Http\Libraries\Assets;
use Illuminate\Http\Request;
use App\Model\StatusRequestOrder;

class RequestOrderStatusController extends Controller
{
  public function index(Request $request)
  {
    $requestorderStatuss = StatusRequestOrder::all();

    $data = [
      'request_order_statuss' => $requestorderStatuss
    ];
    return view('request_order_status.index')->with($data);
  }

  public function add(Request $request)
  {
    return view('request_order_status.add');
  }

  public function edit(Request $request)
  {
    $requestorderStatusId = $_GET['request_order_status_id'];
    $requestorderStatusUpdate = StatusRequestOrder::find($requestorderStatusId);

    $data = [
      'request_order_status' => $requestorderStatusUpdate
    ];
    return view('request_order_status.edit')->with($data);
  }

  public function create(Request $request)
  {
    $data['name'] = $request->input('name');

    $requestorderStatusCreate = StatusRequestOrder::create($data);
    return redirect()->back()->with('message', 'Berhasil Menambahkan Status Request Order');
  }

  public function update(Request $request)
  {
    $requestorderStatusId = $request->input('request_order_status_id');
    $requestorderStatusUpdate = StatusRequestOrder::find($requestorderStatusId);

    $requestorderStatusUpdate->name = $request->input('name');
    $requestorderStatusUpdate->save();

    return redirect()->back()->with('message', 'Berhasil Edit Status Request Order');
  }

  public function delete(Request $request)
  {
    $requestorderStatusId = $request->input('request_order_status_id');
    $requestorderStatusDelete = StatusRequestOrder::find($requestorderStatusId);

    $requestorderStatusDelete->delete();

    return redirect()->back()->with('message', 'Berhasil Menghapus Status Request Order');
  }
}
