<?php
namespace App\Http\Controllers;

use Response;
use App\Http\Libraries\Assets;
use Illuminate\Http\Request;
use App\Model\Brand;
use App\Model\ProductUnit;
use App\Model\Supplier;
use App\Model\Product;

class ProductController extends Controller {

  public function index(Request $request)
  {
    $products = Product::all();

    $data = [
      'products' => $products
    ];
    return view('product.index')->with($data);
  }

  public function add(Request $request)
  {
    $brands = Brand::all();
    $suppliers = Supplier::all();
    $product_units = ProductUnit::all();

    $data = [
      'brands' => $brands,
      'suppliers' => $suppliers,
      'units' => $product_units
    ];
    return view('product.add')->with($data);
  }

  public function edit(Request $request)
  {
    $productId = $_GET['product_id'];
    $productUpdate = Product::find($productId);
    $brands = Brand::all();
    $suppliers = Supplier::all();
    $product_units = ProductUnit::all();

    $data = [
      'product' => $productUpdate,
      'brands' => $brands,
      'suppliers' => $suppliers,
      'units' => $product_units
    ];
    return view('product.edit')->with($data);
  }

  public function create(Request $request)
  {
    $data['brand_id'] = $request->input('brand_id');
    $data['supplier_id'] = $request->input('supplier_id');
    $data['name'] = $request->input('name');
    $data['unit_id'] = $request->input('unit_id');
    $data['price'] = $request->input('price');
    $data['price_nett'] = $request->input('price_nett');
    $data['description'] = ($request->input('description')) ? $request->input('description') : '-' ;

    $productCreate = Product::create($data);
    return redirect()->back()->with('message', 'Berhasil Menambahkan Product');
  }

  public function update(Request $request)
  {
    $productId = $request->input('product_id');
    $productUpdate = Product::find($productId);

    $productUpdate->brand_id = $request->input('brand_id');
    $productUpdate->supplier_id = $request->input('supplier_id');
    $productUpdate->name = $request->input('name');
    $productUpdate->unit_id = $request->input('unit_id');
    $productUpdate->description = $request->input('description');
    $productUpdate->price = $request->input('price');
    $productUpdate->price_nett = $request->input('price_nett');
    $productUpdate->save();

    return redirect()->back()->with('message', 'Berhasil Edit Product');
  }

  public function delete(Request $request)
  {
    $productId = $request->input('product_id');
    $productDelete = Product::find($productId);

    $productDelete->delete();

    return redirect()->back()->with('message', 'Berhasil Menghapus Product');
  }

}
