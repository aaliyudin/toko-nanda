<?php

namespace App\Http\Controllers;

use Response;
use App\Http\Libraries\Assets;
use Illuminate\Http\Request;
use App\Model\Buyer;
use App\Model\Brand;
use App\Model\Supplier;
use App\Model\Product;
use App\Model\RequestOrder;
use App\Model\RequestOrderDetail;

class RequestOrderController extends Controller
{
  public function index(Request $request)
  {
    $requestOrders = RequestOrder::orderBy('created_at', 'DESC')->get();

    $data = [
      'request_orders' => $requestOrders
    ];
    return view('request_order.index')->with($data);
  }

  public function add(Request $request)
  {
    $buyers = Buyer::all();
    $suppliers = Supplier::all();
    $products = Product::all();
    $data = [
      'suppliers' => $suppliers,
      'buyers' => $buyers,
      'products' => $products
    ];
    return view('request_order.add')->with($data);
  }

  public function edit(Request $request)
  {
    $requestOrderId = $_GET['request_order_id'];
    $requestOrderUpdate = RequestOrder::find($requestOrderId);
    $buyers = Buyer::all();
    $suppliers = Supplier::all();
    $products = Product::where('supplier_id', '!=', 0)->get();

    $data = [
      'request_order' => $requestOrderUpdate,
      'suppliers' => $suppliers,
      'buyers' => $buyers,
      'products' => $products
    ];

    return view('request_order.edit')->with($data);
  }

  public function create(Request $request)
  {
    $requestOrder['inv_no'] = 'INV-'.$this->generateRandomString();
    $requestOrder['buyer_id'] = $request->input('buyer_id');
    $requestOrder['discount_order'] = $request->input('discount_order');
    $requestOrder['discount_buyer'] = $request->input('discount_buyer');
    $requestOrder['total_order'] = 0;
    $requestOrder['status_id'] = 1;
    $requestOrderCreate = RequestOrder::create($requestOrder);

    $productLength = count($request->input('product_id'));
    for ($i=0; $i < $productLength; $i++) {
      $requestOrderDetail['request_order_id'] = $requestOrderCreate->id;
      $requestOrderDetail['product_id'] = $request->input('product_id')[$i];
      $requestOrderDetail['qty'] = $request->input('qty')[$i];
      $requestOrderDetail['discount_product'] = $request->input('discount_product')[$i];
      $requestOrderDetail['discount_brand'] = Product::find($request->input('product_id')[$i])->brand->discount;
      $requestOrderDetail['total_price'] = $request->input('total_product_price')[$i];
      $requestOrderDetailCreate = RequestOrderDetail::create($requestOrderDetail);
    }

    $requestOrder = $requestOrderCreate;
    $requestOrderDetail = $requestOrder->detail;
    $requestOrderDetails = [];
    $totalOrderStruk = 0;
    foreach ($requestOrderDetail as $key => $value) {
      $requestOrderDetails[$key]['name'] = $value->product->name;
      $requestOrderDetails[$key]['ro_id'] = $value->request_order_id;
      $requestOrderDetails[$key]['brand'] = $value->product->brand->name;
      $requestOrderDetails[$key]['brand_id'] = $value->product->brand->id;
      $requestOrderDetails[$key]['price'] = $value->product->price;
      $requestOrderDetails[$key]['qty'] = $value->qty;
      $requestOrderDetails[$key]['discount'] = $value->discount_product;
      $requestOrderDetails[$key]['discount_brand'] = $value->discount_brand;
      $requestOrderDetails[$key]['total'] = $value->total_price;
      $totalOrderStruk += $value->total_price;
    }
    $result = array();
    $totalO = 0;
    foreach ($requestOrderDetails as $detail) {
      $result[$detail['brand']]['product'][] = $detail;
      $discoutBrand = $detail['discount_brand'];
      $discoutBuyer = RequestOrder::find($detail['ro_id'])->discount_buyer;
      $result[$detail['brand']]['discount'] = $discoutBrand;
      $result[$detail['brand']]['discount_buyer'] = $discoutBuyer;
      $totalO  += $detail['total'];
      $result[$detail['brand']]['total'] = $totalO;
      $discVal = $totalO * $discoutBrand / 100;
      $result[$detail['brand']]['discount_val'] = $discVal;
      $totalBrandVal = $totalO - $discVal;
      $result[$detail['brand']]['total_after_disc'] = $totalBrandVal;
      $discBuyer = $totalBrandVal * $discoutBuyer / 100;
      $result[$detail['brand']]['discount_buyer_val'] = $discBuyer;
      $result[$detail['brand']]['total_after_disc_buyer'] = $totalBrandVal - $discBuyer;
    }
    $totalAll = 0;
    $totalAllBuyer = 0;
    foreach ($result as $key => $value) {
      $totalAll += $value['total_after_disc'];
      $totalAllBuyer += $value['total_after_disc_buyer'];
    }
    $requestOrder['buyer'] = $requestOrder->buyer->name;
    $requestOrder['total_order'] = $totalAll;

    $requestOrderUpdate = RequestOrder::find($requestOrderCreate->id);
    $requestOrderUpdate->total_order = $totalAll;
    $requestOrderUpdate->save();

    return redirect()->back()->with('message', 'Berhasil Membuat Permintaan');
  }

  public function update(Request $request)
  {
    $requestOrderId = $request->input('request_order_id');
    $requestOrderUpdate = RequestOrder::find($requestOrderId);

    $requestOrderUpdate->buyer_id = $request->input('buyer_id');
    $requestOrderUpdate->discount_order = $request->input('discount_order');
    $requestOrderUpdate->discount_buyer = $request->input('discount_buyer');
    $requestOrderUpdate->total_order = 0;
    $requestOrderUpdate->save();

    RequestOrderDetail::where('request_order_id', $requestOrderId)->delete();
    $productLength = count($request->input('product_id'));
    for ($i=0; $i < $productLength; $i++) {
      $requestOrderDetail['request_order_id'] = $requestOrderUpdate->id;
      $requestOrderDetail['product_id'] = $request->input('product_id')[$i];
      $requestOrderDetail['qty'] = $request->input('qty')[$i];
      $requestOrderDetail['discount_product'] = $request->input('discount_product')[$i];
      $requestOrderDetail['discount_brand'] = Product::find($request->input('product_id')[$i])->brand->discount;
      $requestOrderDetail['total_price'] = $request->input('total_product_price')[$i];
      $requestOrderDetailCreate = RequestOrderDetail::create($requestOrderDetail);
    }

    $requestOrder = $requestOrderUpdate;
    $requestOrderDetail = $requestOrder->detail;
    $requestOrderDetails = [];
    $totalOrderStruk = 0;
    foreach ($requestOrderDetail as $key => $value) {
      $requestOrderDetails[$key]['name'] = $value->product->name;
      $requestOrderDetails[$key]['ro_id'] = $value->request_order_id;
      $requestOrderDetails[$key]['brand'] = $value->product->brand->name;
      $requestOrderDetails[$key]['brand_id'] = $value->product->brand->id;
      $requestOrderDetails[$key]['price'] = $value->product->price;
      $requestOrderDetails[$key]['qty'] = $value->qty;
      $requestOrderDetails[$key]['discount'] = $value->discount_product;
      $requestOrderDetails[$key]['discount_brand'] = $value->discount_brand;
      $requestOrderDetails[$key]['total'] = $value->total_price;
      $totalOrderStruk += $value->total_price;
    }
    $result = array();
    $totalO = 0;
    foreach ($requestOrderDetails as $detail) {
      $result[$detail['brand']]['product'][] = $detail;
      $discoutBrand = $detail['discount_brand'];
      $discoutBuyer = RequestOrder::find($detail['ro_id'])->discount_buyer;
      $result[$detail['brand']]['discount'] = $discoutBrand;
      $result[$detail['brand']]['discount_buyer'] = $discoutBuyer;
      $totalO  += $detail['total'];
      $result[$detail['brand']]['total'] = $totalO;
      $discVal = $totalO * $discoutBrand / 100;
      $result[$detail['brand']]['discount_val'] = $discVal;
      $totalBrandVal = $totalO - $discVal;
      $result[$detail['brand']]['total_after_disc'] = $totalBrandVal;
      $discBuyer = $totalBrandVal * $discoutBuyer / 100;
      $result[$detail['brand']]['discount_buyer_val'] = $discBuyer;
      $result[$detail['brand']]['total_after_disc_buyer'] = $totalBrandVal - $discBuyer;
    }
    $totalAll = 0;
    $totalAllBuyer = 0;
    foreach ($result as $key => $value) {
      $totalAll += $value['total_after_disc'];
      $totalAllBuyer += $value['total_after_disc_buyer'];
    }
    $requestOrder['buyer'] = $requestOrder->buyer->name;
    $requestOrder['total_order'] = $totalAll;

    $requestOrderU = RequestOrder::find($requestOrderUpdate->id);
    $requestOrderU->total_order = $totalAll;
    $requestOrderU->save();

    return redirect()->back()->with('message', 'Berhasil Edit Permintaan');
  }

  public function delete(Request $request)
  {
    $requestOrderId = $request->input('request_order_id');
    $requestOrderDelete = RequestOrder::find($requestOrderId);

    $requestOrderDelete->delete();

    return redirect()->back()->with('message', 'Berhasil Menghapus Permintaan');
  }

  public function generateRandomString($length = 10) {
    $characters = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
      $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
  }
}
