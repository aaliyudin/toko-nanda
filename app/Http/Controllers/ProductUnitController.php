<?php

namespace App\Http\Controllers;

use Response;
use App\Http\Libraries\Assets;
use Illuminate\Http\Request;
use App\Model\ProductUnit;

class ProductUnitController extends Controller
{
  public function index(Request $request)
  {
    $productUnits = ProductUnit::all();

    $data = [
      'product_units' => $productUnits
    ];
    return view('product_unit.index')->with($data);
  }

  public function add(Request $request)
  {
    return view('product_unit.add');
  }

  public function edit(Request $request)
  {
    $productUnitId = $_GET['product_unit_id'];
    $productUnitUpdate = ProductUnit::find($productUnitId);

    $data = [
      'product_unit' => $productUnitUpdate
    ];
    return view('product_unit.edit')->with($data);
  }

  public function create(Request $request)
  {
    $data['name'] = $request->input('name');

    $productUnitCreate = ProductUnit::create($data);
    return redirect()->back()->with('message', 'Berhasil Menambahkan Satuan Product');
  }

  public function update(Request $request)
  {
    $productUnitId = $request->input('product_unit_id');
    $productUnitUpdate = ProductUnit::find($productUnitId);

    $productUnitUpdate->name = $request->input('name');
    $productUnitUpdate->save();

    return redirect()->back()->with('message', 'Berhasil Edit Satuan Product');
  }

  public function delete(Request $request)
  {
    $productUnitId = $request->input('product_unit_id');
    $productUnitDelete = ProductUnit::find($productUnitId);

    $productUnitDelete->delete();

    return redirect()->back()->with('message', 'Berhasil Menghapus Satuan Product');
  }
}
