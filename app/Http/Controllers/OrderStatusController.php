<?php

namespace App\Http\Controllers;

use Response;
use App\Http\Libraries\Assets;
use Illuminate\Http\Request;
use App\Model\StatusOrder;

class OrderStatusController extends Controller
{
  public function index(Request $request)
  {
    $orderStatuss = StatusOrder::all();

    $data = [
      'order_statuss' => $orderStatuss
    ];
    return view('order_status.index')->with($data);
  }

  public function add(Request $request)
  {
    return view('order_status.add');
  }

  public function edit(Request $request)
  {
    $orderStatusId = $_GET['order_status_id'];
    $orderStatusUpdate = StatusOrder::find($orderStatusId);

    $data = [
      'order_status' => $orderStatusUpdate
    ];
    return view('order_status.edit')->with($data);
  }

  public function create(Request $request)
  {
    $data['name'] = $request->input('name');

    $orderStatusCreate = StatusOrder::create($data);
    return redirect()->back()->with('message', 'Berhasil Menambahkan Order Status');
  }

  public function update(Request $request)
  {
    $orderStatusId = $request->input('order_status_id');
    $orderStatusUpdate = StatusOrder::find($orderStatusId);

    $orderStatusUpdate->name = $request->input('name');
    $orderStatusUpdate->save();

    return redirect()->back()->with('message', 'Berhasil Edit Order Status');
  }

  public function delete(Request $request)
  {
    $orderStatusId = $request->input('order_status_id');
    $orderStatusDelete = StatusOrder::find($orderStatusId);

    $orderStatusDelete->delete();

    return redirect()->back()->with('message', 'Berhasil Menghapus Order Status');
  }
}
