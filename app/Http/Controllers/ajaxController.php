<?php

namespace App\Http\Controllers;

use Response;
use App\Http\Libraries\Assets;
use Illuminate\Http\Request;
use App\Model\Brand;
use App\Model\ProductUnit;
use App\Model\Supplier;
use App\Model\Product;
use App\Model\Order;
use App\Model\OrderDetail;
use App\Model\RequestOrder;
use App\Model\RequestOrderDetail;
use App\Model\Buyer;
use App\Model\DiscountBrand;

class ajaxController extends Controller
{
  public function prodcutBySupplier(Request $request)
  {
    $product = Product::where('supplier_id', $request->input('supplier_id'))->get();
    return $product;
  }
  public function discountByBuyer(Request $request)
  {
    $discoutBrand = DiscountBrand::where('buyer_id', $request->input('buyer_id'))->get();
    return $discoutBrand;
  }
  public function allProduct(Request $request)
  {
    $product = Product::where('supplier_id', '!=', 0)->get();
    return $product;
  }
  public function prodcutGetPrice(Request $request)
  {
    $product = Product::find($request->input('product_id'));
    $discBuyer = DiscountBrand::where('buyer_id', $request->input('buyer_id'))->where('brand_id', $product->brand_id)->first();

    $discoutBrand = $product->price * $discBuyer['discount_1'] / 100;
    $productAftrDisc = $product->price - $discoutBrand;
    $products = [
      'price' => $product->price,
      'discount' => $discBuyer['discount_1'],
      'price_after_discount' => $productAftrDisc
    ];
    return $products;
  }
  public function requestOrderBySupplier(Request $request)
  {
    $requestOrder = RequestOrder::where('supplier_id', $request->input('supplier_id'))->where('status_id', 1)->get();
    $data = [
      'requestOrders' => $requestOrder
    ];
    return view('template.request_order_by_supplier')->with($data);
  }

  public function getProductTotalByRequestOrder(Request $request)
  {
    $requestOrderLength = count($request->input('request_order_id'));
    $requestOrder = [];
    $requestOrderId = [];
    for ($i=0; $i < $requestOrderLength; $i++) {
      $requestOrder[] = RequestOrderDetail::where('request_order_id',$request->input('request_order_id')[$i])->get();
      $requestOrderId[] = $request->input('request_order_id')[$i];
    }
    $requestOrderDetail = [];

    foreach ($requestOrder as $key => $requestOrderDetails) {
      foreach ($requestOrderDetails as $key => $reqDetail) {
        $requestOrderDetail[$reqDetail['product_id']][] = $reqDetail;
      }
    }
    $req = [];
    foreach ($requestOrderDetail as $key => $value) {
      $qty = 0;
      foreach ($value as $t => $val) {
        $qty += $val['qty'];
      }
      $req[$key]['qty'] = $qty;
      $product = Product::find($key);
      $req[$key]['name'] = $product->name;
      $req[$key]['brand'] = $product->brand->name;
      $req[$key]['price'] = $product->price_nett;
      $req[$key]['total'] = $product->price_nett * $qty;
    }

    $data = [
      'requestOrderDetails' => $req,
      'requestOrderIds' => $requestOrderId
    ];
    return view('template.product_request_order')->with($data);
    // foreach ($requestOrderDetails as $key => $val) {
    //   if (isset($requestOrderDetail[$val->product_id])) {
    //     $requestOrderDetail[$val->product_id]['name'] = Product::find($val->product_id)->name;
    //     $requestOrderDetail[$val->product_id]['price'] = Product::find($val->product_id)->price_nett;
    //     $qty += (int)$val->qty;
    //     $requestOrderDetail[$val->product_id]['qty'] = $qty;
    //     $requestOrderDetail[$val->product_id]['total'] = Product::find($val->product_id)->price_nett * $qty;
    //   } else {
    //     $requestOrderDetail[$val->product_id]['name'] = Product::find($val->product_id)->name;
    //     $requestOrderDetail[$val->product_id]['price'] = Product::find($val->product_id)->price_nett;
    //     $qty = (int)$val->qty;
    //     $requestOrderDetail[$val->product_id]['qty'] = $qty;
    //     $requestOrderDetail[$val->product_id]['total'] = Product::find($val->product_id)->price_nett * $qty;
    //   }
    // }
  }

  public function showOrderDetail(Request $request)
  {
    $order = Order::find($request->input('order_id'));
    $orderDetail = OrderDetail::where('order_id', $request->input('order_id'))->get();
    $orderDetails = [];
    foreach ($orderDetail as $key => $value) {
      $orderDetails[$key]['name'] = $value->product->name;
      $orderDetails[$key]['brand'] = $value->product->brand->name;
      $orderDetails[$key]['price'] = $value->product->price_nett;
      $orderDetails[$key]['qty'] = $value->qty;
      $orderDetails[$key]['total'] = $value->total_price;
    }
    $order['supplier'] = $order->supplier->name;
    $data = [
      'order' => $order,
      'order_details' => $orderDetails
    ];
    return $data;
  }

  public function showRequestOrderDetail(Request $request)
  {
    $requestOrder = RequestOrder::find($request->input('request_order_id'));
    $requestOrderDetail = RequestOrderDetail::where('request_order_id', $request->input('request_order_id'))->get();
    $requestOrderDetails = [];
    $totalOrderStruk = 0;
    foreach ($requestOrderDetail as $key => $value) {
      $requestOrderDetails[$key]['name'] = $value->product->name;
      $requestOrderDetails[$key]['ro_id'] = $value->request_order_id;
      $requestOrderDetails[$key]['brand'] = $value->product->brand->name;
      $requestOrderDetails[$key]['brand_id'] = $value->product->brand->id;
      $requestOrderDetails[$key]['price'] = $value->product->price;
      $requestOrderDetails[$key]['qty'] = $value->qty;
      $requestOrderDetails[$key]['discount'] = $value->discount_product;
      $requestOrderDetails[$key]['discount_brand'] = $value->discount_brand;
      $requestOrderDetails[$key]['total'] = $value->total_price;
      $totalOrderStruk += $value->total_price;
    }
    $result = array();
    $totalO = 0;
    foreach ($requestOrderDetails as $detail) {
      $result[$detail['brand']]['product'][] = $detail;
      $discoutBrand = $detail['discount_brand'];
      $discBuyBrand = DiscountBrand::where('buyer_id', $requestOrder->buyer->id)->where('brand_id', $detail['brand_id'])->first();
      $discoutBuyer = $discBuyBrand['discount_2'];
      $result[$detail['brand']]['discount'] = $discoutBrand;
      $result[$detail['brand']]['discount_buyer'] = $discoutBuyer;
      $totalO  += $detail['total'];
      $result[$detail['brand']]['total'] = $totalO;
      $discVal = $totalO * $discoutBrand / 100;
      $result[$detail['brand']]['discount_val'] = $discVal;
      $totalBrandVal = $totalO - $discVal;
      $result[$detail['brand']]['total_after_disc'] = $totalBrandVal;
      $discBuyer = $totalBrandVal * $discoutBuyer / 100;
      $result[$detail['brand']]['discount_buyer_val'] = $discBuyer;
      $result[$detail['brand']]['total_after_disc_buyer'] = $totalBrandVal - $discBuyer;
    }
    $totalAll = 0;
    $totalAllBuyer = 0;
    foreach ($result as $key => $value) {
      $totalAll += $value['total_after_disc'];
      $totalAllBuyer += $value['total_after_disc_buyer'];
    }
    $requestOrder['buyer'] = $requestOrder->buyer->name;
    $requestOrder['total_order'] = $totalAll;
    $requestOrder['total_order_after_disc'] = $totalAllBuyer;
    $data = [
      'request_order' => $requestOrder,
      'request_order_details' => $result
    ];
    // return $data;
    return view('ajax.request_order_modal')->with($data);
  }

  public function printStrukBuyer(Request $request)
  {
    $requestOrder = RequestOrder::find($request->input('request_order_id'));
    $requestOrderDetail = RequestOrderDetail::where('request_order_id', $request->input('request_order_id'))->get();
    $requestOrderDetails = [];
    $totalOrderStruk = 0;
    foreach ($requestOrderDetail as $key => $value) {
      $requestOrderDetails[$key]['name'] = $value->product->name;
      $requestOrderDetails[$key]['ro_id'] = $value->request_order_id;
      $requestOrderDetails[$key]['brand'] = $value->product->brand->name;
      $requestOrderDetails[$key]['brand_id'] = $value->product->brand->id;
      $requestOrderDetails[$key]['price'] = $value->product->price;
      $requestOrderDetails[$key]['qty'] = $value->qty;
      $requestOrderDetails[$key]['discount'] = $value->discount_product;
      $requestOrderDetails[$key]['discount_brand'] = $value->discount_brand;
      $requestOrderDetails[$key]['total'] = $value->total_price;
      $totalOrderStruk += $value->total_price;
    }
    $result = array();
    $totalO = 0;
    foreach ($requestOrderDetails as $detail) {
      $result[$detail['brand']]['product'][] = $detail;
      $discoutBrand = $detail['discount_brand'];
      $discBuyBrand = DiscountBrand::where('buyer_id', $requestOrder->buyer->id)->where('brand_id', $detail['brand_id'])->first();
      $discoutBuyer = $discBuyBrand['discount_2'];
      $result[$detail['brand']]['discount'] = $discoutBrand;
      $result[$detail['brand']]['discount_buyer'] = $discoutBuyer;
      $totalO  += $detail['total'];
      $result[$detail['brand']]['total'] = $totalO;
      $discVal = $totalO * $discoutBrand / 100;
      $result[$detail['brand']]['discount_val'] = $discVal;
      $totalBrandVal = $totalO - $discVal;
      $result[$detail['brand']]['total_after_disc'] = $totalBrandVal;
      $discBuyer = $totalBrandVal * $discoutBuyer / 100;
      $result[$detail['brand']]['discount_buyer_val'] = $discBuyer;
      $result[$detail['brand']]['total_after_disc_buyer'] = $totalBrandVal - $discBuyer;
    }
    $totalAll = 0;
    $totalAllBuyer = 0;
    foreach ($result as $key => $value) {
      $totalAll += $value['total_after_disc'];
      $totalAllBuyer += $value['total_after_disc_buyer'];
    }
    $requestOrder['buyer'] = $requestOrder->buyer->name;
    $requestOrder['total_order'] = $totalAll;
    $requestOrder['total_order_after_disc'] = $totalAllBuyer;
    $data = [
      'request_order' => $requestOrder,
      'request_order_details' => $result
    ];
    return view('template.struk_pembeli')->with($data);
  }

  public function printStruk(Request $request)
  {
    $requestOrder = RequestOrder::find($request->input('request_order_id'));
    $requestOrderDetail = RequestOrderDetail::where('request_order_id', $request->input('request_order_id'))->get();
    $requestOrderDetails = [];
    $totalOrderStruk = 0;
    foreach ($requestOrderDetail as $key => $value) {
      $requestOrderDetails[$key]['name'] = $value->product->name;
      $requestOrderDetails[$key]['ro_id'] = $value->request_order_id;
      $requestOrderDetails[$key]['brand'] = $value->product->brand->name;
      $requestOrderDetails[$key]['brand_id'] = $value->product->brand->id;
      $requestOrderDetails[$key]['price'] = $value->product->price;
      $requestOrderDetails[$key]['qty'] = $value->qty;
      $requestOrderDetails[$key]['discount'] = $value->discount_product;
      $requestOrderDetails[$key]['discount_brand'] = $value->discount_brand;
      $requestOrderDetails[$key]['total'] = $value->total_price;
      $totalOrderStruk += $value->total_price;
    }
    $result = array();
    $totalO = 0;
    foreach ($requestOrderDetails as $detail) {
      $result[$detail['brand']]['product'][] = $detail;
      $discoutBrand = $detail['discount_brand'];
      $discBuyBrand = DiscountBrand::where('buyer_id', $requestOrder->buyer->id)->where('brand_id', $detail['brand_id'])->first();
      $discoutBuyer = $discBuyBrand['discount_2'];
      $result[$detail['brand']]['discount'] = $discoutBrand;
      $result[$detail['brand']]['discount_buyer'] = $discoutBuyer;
      $totalO  += $detail['total'];
      $result[$detail['brand']]['total'] = $totalO;
      $discVal = $totalO * $discoutBrand / 100;
      $result[$detail['brand']]['discount_val'] = $discVal;
      $totalBrandVal = $totalO - $discVal;
      $result[$detail['brand']]['total_after_disc'] = $totalBrandVal;
      $discBuyer = $totalBrandVal * $discoutBuyer / 100;
      $result[$detail['brand']]['discount_buyer_val'] = $discBuyer;
      $result[$detail['brand']]['total_after_disc_buyer'] = $totalBrandVal - $discBuyer;
    }
    $totalAll = 0;
    $totalAllBuyer = 0;
    foreach ($result as $key => $value) {
      $totalAll += $value['total_after_disc'];
      $totalAllBuyer += $value['total_after_disc_buyer'];
    }
    $requestOrder['buyer'] = $requestOrder->buyer->name;
    $requestOrder['total_order'] = $totalAll;
    $requestOrder['total_order_after_disc'] = $totalAllBuyer;
    $data = [
      'request_order' => $requestOrder,
      'request_order_details' => $result
    ];
    return view('template.struk')->with($data);
  }
}
