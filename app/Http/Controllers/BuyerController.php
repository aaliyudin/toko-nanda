<?php
namespace App\Http\Controllers;

use Response;
use App\Http\Libraries\Assets;
use Illuminate\Http\Request;
use App\Model\Buyer;
use App\Model\Brand;
use App\Model\DiscountBrand;

class BuyerController extends Controller {

  public function index(Request $request)
  {
    $buyers = Buyer::all();

    $data = [
      'buyers' => $buyers
    ];
    return view('buyer.index')->with($data);
  }

  public function add(Request $request)
  {
    $brand = Brand::all();

    $data = [
      'brands' => $brand
    ];
    return view('buyer.add')->with($data);
  }

  public function edit(Request $request)
  {
    $buyerId = $_GET['buyer_id'];
    $buyerUpdate = Buyer::find($buyerId);
    $brand = Brand::all();
    $discountBrand = DiscountBrand::where('buyer_id', $buyerId)->get();
    // dd($discountBrand);
    $data = [
      'buyer' => $buyerUpdate,
      'brands' => $brand,
      'discount_brand' => $discountBrand
    ];
    return view('buyer.edit')->with($data);
  }

  public function create(Request $request)
  {
    $data['name'] = $request->input('name');
    $data['phone'] = $request->input('phone');
    $data['address'] = $request->input('address');
    $data['company'] = $request->input('company');
    $buyerCreate = Buyer::create($data);
    foreach ($request->input('diskon-brand') as $key => $diskonBrand) {
      $dataDisk['buyer_id'] = $buyerCreate->id;
      $dataDisk['brand_id'] = $diskonBrand['id'];
      foreach ($diskonBrand['disk'] as $key => $disk) {
        $t = (int)$key+1;
        $dataDisk['discount_'. $t .''] = ($disk == null) ? 0 : (int)$disk;
      }
      DiscountBrand::create($dataDisk);
    }

    return redirect()->back()->with('message', 'Berhasil Menambahkan Pembeli');
  }

  public function update(Request $request)
  {
    $buyerId = $request->input('buyer_id');
    $buyerUpdate = Buyer::find($buyerId);

    $buyerUpdate->name = $request->input('name');
    $buyerUpdate->phone = $request->input('phone');
    $buyerUpdate->address = $request->input('address');
    $buyerUpdate->company = $request->input('company');
    $buyerUpdate->save();

    DiscountBrand::where('buyer_id', $buyerId)->delete();
    foreach ($request->input('diskon-brand') as $key => $diskonBrand) {
      $dataDisk['buyer_id'] = $buyerId;
      $dataDisk['brand_id'] = $diskonBrand['id'];
      foreach ($diskonBrand['disk'] as $key => $disk) {
        $t = (int)$key+1;
        $dataDisk['discount_'. $t .''] = ($disk == null) ? 0 : $disk;
      }
      DiscountBrand::create($dataDisk);
    }

    return redirect()->back()->with('message', 'Berhasil Edit Pembeli');
  }

  public function delete(Request $request)
  {
    $buyerId = $request->input('buyer_id');
    $buyerDelete = Buyer::find($buyerId);

    $buyerDelete->delete();

    return redirect()->back()->with('message', 'Berhasil Menghapus Pembeli');
  }

}
