<?php

namespace App\Http\Controllers;

use Response;
use App\Http\Libraries\Assets;
use Illuminate\Http\Request;
use App\Model\Brand;

class BrandController extends Controller
{
  public function index(Request $request)
  {
    $brands = Brand::all();

    $data = [
      'brands' => $brands
    ];
    return view('brand.index')->with($data);
  }

  public function add(Request $request)
  {
    return view('brand.add');
  }

  public function edit(Request $request)
  {
    $brandId = $_GET['brand_id'];
    $brandUpdate = Brand::find($brandId);

    $data = [
      'brand' => $brandUpdate
    ];
    return view('brand.edit')->with($data);
  }

  public function create(Request $request)
  {
    $data['name'] = $request->input('name');
    $data['discount'] = $request->input('discount');

    $brandCreate = Brand::create($data);
    return redirect()->back()->with('message', 'Berhasil Menambahkan Brand');
  }

  public function update(Request $request)
  {
    $brandId = $request->input('brand_id');
    $brandUpdate = Brand::find($brandId);

    $brandUpdate->name = $request->input('name');
    $brandUpdate->discount = $request->input('discount');
    $brandUpdate->save();

    return redirect()->back()->with('message', 'Berhasil Edit Brand');
  }

  public function delete(Request $request)
  {
    $brandId = $request->input('brand_id');
    $brandDelete = Brand::find($brandId);

    $brandDelete->delete();

    return redirect()->back()->with('message', 'Berhasil Menghapus Brand');
  }
}
