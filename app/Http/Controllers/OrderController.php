<?php

namespace App\Http\Controllers;

use Response;
use App\Http\Libraries\Assets;
use Illuminate\Http\Request;
use App\Model\Buyer;
use App\Model\Supplier;
use App\Model\Product;
use App\Model\RequestOrder;
use App\Model\RequestOrderDetail;
use App\Model\Order;
use App\Model\OrderDetail;
use SoapClient;

class OrderController extends Controller
{
  public function index(Request $request)
  {
    $orders = Order::all();
    $data = [
      'orders' => $orders
    ];

    return view('order.index')->with($data);
  }

  public function add(Request $request)
  {
    $requestOrder = RequestOrder::where('status_id', 1)->orderBy('created_at', 'DESC')->get();
    $data = [
      'request_orders' => $requestOrder
    ];
    return view('order.add')->with($data);
  }

  public function edit(Request $request)
  {
    // code...
  }

  public function create(Request $request)
  {
    $total = 0;
    foreach ($request['total'] as $key => $value) {
      $total += $value;
    }
    $order['inv_no'] = 'INV-'.$this->generateRandomString();
    $order['supplier_id'] = 1;
    $order['total_price'] = $total;
    $order['status_id'] = 1;
    $orderCreate = Order::create($order);

    foreach ($request->input('product_id') as $key => $value) {
      $orderDetail['order_id'] = $orderCreate->id;
      $orderDetail['product_id'] = $value;
      $orderDetail['qty'] = $request->input('qty')[$key];
      $orderDetail['total_price'] = $request->input('total')[$key];
      $orderDetailCreate = OrderDetail::create($orderDetail);
    }

    foreach ($request->input('checkbox_request_order') as $key => $value) {
      $requestOrder = RequestOrder::find($value);
      $requestOrder->status_id = 2;
      $requestOrder->save();
    }

    return redirect()->back()->with('message', 'Berhasil Membuat Order');
  }

  public function update(Request $request)
  {
    // code...
  }

  public function delete(Request $request)
  {
    // code...
  }

  public function generateRandomString($length = 10) {
    $characters = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
      $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
  }

  public function test()
  {

    $movies = array(
      array(
        "title" => "Rear Window",
        "director" => "Alfred Hitchcock",
        "year" => 1973
      ),
      array(
        "title" => "Full Metal Jacket",
        "director" => "Stanley Kubrick",
        "year" => 1987
      ),
      array(
        "title" => "Mean Streets",
        "director" => "Martin Scorsese",
        "year" => 1973
      ),
      array(
        "title" => "Full Metal Jacket",
        "director" => "Stanley Kubrick",
        "year" => 1987
      ),
      array(
        "title" => "Full Metal Jacket",
        "director" => "Stanley Kubrick",
        "year" => 1987
      ),
      array(
        "title" => "Full Metal Jacket",
        "director" => "Stanley Kubrick",
        "year" => 1981
      ),
      array(
        "title" => "Full Metal Jacket",
        "director" => "Stanley Kubrick",
        "year" => 1981
      )
    );

    $result = array();
    foreach ($movies as $element) {
        $result[$element['year']][] = $element;
    }
    // dd($result);
    foreach ($result as $key => $value) {
      // print_r($key);
      // print_r($value[0]['year']);
      foreach ($value as $k => $v) {
        // print_r($v);
      }
    }

  }
}
