@extends('layout.app')

@section('title', 'Tambah Brand')

@section('css')
@endsection

@section('content')
@if(session()->has('message'))
    <div class="alert alert-success">
        {{ session()->get('message') }}
    </div>
@endif
<div class="box box-primary">
  <div class="box-header with-border">
    <h3 class="box-title">Tambah Brand</h3>
  </div>
  <!-- /.box-header -->
  <!-- form start -->
  <form role="form" action="{{route('create_brand')}}" method="post">
    @csrf
    <div class="box-body">
      <div class="form-group">
        <label for="name">Nama Brand</label>
        <input required type="text" name="name" class="form-control" id="name" placeholder="Masukan Nama Brand">
      </div>
    </div>
    <div class="box-body">
      <div class="form-group">
        <label for="name">Discount</label>
        <input required type="number" name="discount" step="0.01" class="form-control" id="discount">
      </div>
    </div>
    <!-- /.box-body -->

    <div class="box-footer">
      <button type="submit" class="btn btn-primary">Submit</button>
    </div>
  </form>
</div>
@endsection
