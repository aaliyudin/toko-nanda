@extends('layout.app')

@section('title', 'Tambah Brand')

@section('css')
@endsection

@section('content')
@if(session()->has('message'))
    <div class="alert alert-success">
        {{ session()->get('message') }}
    </div>
@endif
<div class="box box-primary">
  <div class="box-header with-border">
    <h3 class="box-title">Edit Brand</h3>
  </div>
  <!-- /.box-header -->
  <!-- form start -->
  <form role="form" action="{{route('update_brand')}}" method="post">
    @csrf
    <div class="box-body">
      <div class="form-group">
        <label for="name">Nama Brand</label>
        <input required type="text" name="name" class="form-control" id="name" value="{{$brand->name}}" placeholder="Masukan Nama Brand">
      </div>
      <div class="form-group">
        <label for="name">Discount</label>
        <input required type="text" name="discount" class="form-control" id="discount" value="{{$brand->discount}}" placeholder="Masukan Nama Brand">
      </div>
      <input type="hidden" name="brand_id" value="{{$brand->id}}">
    </div>
    <!-- /.box-body -->

    <div class="box-footer">
      <button type="submit" class="btn btn-primary">Submit</button>
    </div>
  </form>
</div>
@endsection
