<div class="modal-content">
  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
      <span aria-hidden="true">×</span></button>
      <h4 class="modal-title">{{$request_order->inv_no}} - {{$request_order->buyer}}</h4>
      <span class="modal-date">{{$request_order->created_at}}</span>
    </div>
    <div class="modal-body">
      @foreach($request_order_details as $brand => $ro_detail)
      <table class="table table-bordered">
        <thead>
          <tr>
            <th style="width: 10px">#</th>
            <th>Nama Product</th>
            <th>Harga</th>
            <th style="width: 100px">Qty</th>
            <th style="width: 100px">Discount</th>
            <th style="text-align:right">Total</th>
          </tr>
        </thead>
        <tbody id="request_order_details_render">
          <?php $i = 1 ?>
          @foreach($ro_detail['product'] as $product)
          <tr>
            <td>{{$i}}</td>
            <td>{{$product['name']}}</td>
            <td>{{number_format($product['price'],2,",",".")}}</td>
            <td>{{$product['qty']}}</td>
            <td>{{$product['discount']}}%</td>
            <td style="text-align:right" >{{number_format($product['total'],2,",",".")}}</td>
          </tr>
          <?php $i++ ?>
          @endforeach
        </tbody>
        <tfoot>
          <tr>
            <th colspan="2">Total {{$brand}}</th>
            <th></th>
            <th></th>
            <th></th>
            <th style="text-align:right" id="render_total_request_order_details_render">{{number_format($ro_detail['total'],2,",",".")}}</th>
          </tr>
          <tr>
            <th></th>
            <th></th>
            <th></th>
            <th colspan="2">Jumlah {{$brand}}</th>
            <th style="text-align:right" id="render_total_request_order_details_render">{{number_format($ro_detail['total_after_disc'],2,",",".")}}</th>
          </tr>
        </tfoot>
      </table>
      @endforeach

      <ul class="list-group list-group-unbordered" id="render_discount_list">
        <li class="list-group-item"><b>Jumlah yang harus dibayar</b><b class="pull-right">{{number_format($request_order->total_order,2,",",".")}}</b></li>
      </ul>

      <button type="button" class="btn" data-toggle="collapse" data-target="#demo">Pembeli</button>
      <div id="demo" class="collapse">

        @foreach($request_order_details as $brand => $ro_detail)
        <ul class="list-group list-group-unbordered" id="render_discount_list">
          <li class="list-group-item"><span>Tambahan disc {{$brand}} {{$ro_detail['discount_buyer']}}%</span><b class="pull-right">{{number_format($ro_detail['discount_buyer_val'],2,",",".")}}</b></li>
          <li class="list-group-item"><span>Jumlah {{$brand}}</span><b class="pull-right">{{number_format($ro_detail['total_after_disc_buyer'],2,",",".")}}</b></li>
        </ul>
        @endforeach

        <ul class="list-group list-group-unbordered" id="render_discount_list">
          <li class="list-group-item"><span>Jumlah tagihan <b>{{$request_order->buyer}}</b></span><b class="pull-right">{{number_format($request_order->total_order_after_disc,2,",",".")}}</b></li>
        </ul>
      </div>
    </div>
    <div class="modal-footer">
      <input type="hidden" id="print_id" value="{{$request_order->id}}">
      <button type="button" class="btn btn-danger pull-left" data-dismiss="modal">Tutup</button>
      <button type="button" id="print_for_buyer" class="btn btn-primary">Print Struk Pembeli</button>
      <button type="button" id="print" class="btn btn-success">Print Struk</button>
    </div>
  </div>
  <script type="text/javascript">
  $('#print_for_buyer').on('click', function(){
    var request_order_id = $('#print_id').val();
    var url = "{{route('prnit_struk_buyer')}}?request_order_id="+request_order_id+"";
    window.open(url, '_blank');
    window.focus();
  });
  $('#print').on('click', function(){
    var request_order_id = $('#print_id').val();
    var url = "{{route('prnit_struk')}}?request_order_id="+request_order_id+"";
    window.open(url, '_blank');
    window.focus();
  });
  </script>
