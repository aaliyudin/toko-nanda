
@extends('layout.app')

@section('title', 'List Satuan')

@section('css')
<!-- DataTables -->
<link rel="stylesheet" href="bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
@endsection
@section('content')
<div class="box">
  <div class="box-header">
    <h3 class="box-title">List Satuan</h3>
  </div>
  <!-- /.box-header -->
  <div class="box-body">
    <table id="example2" class="table table-bordered table-striped">
      <thead>
      <tr>
        <th>Satuan ID</th>
        <th>Nama Satuan</th>
        <th>Action</th>
      </tr>
      </thead>
      <tbody>
      @foreach($product_units as $product_unit)
      <tr>
        <td>{{$product_unit->id}}</td>
        <td>{{$product_unit->name}}</td>
        <td><a href="{{route('edit_product_unit')}}?product_unit_id={{$product_unit->id}}">Edit</a></td>
      </tr>
      @endforeach
      </tbody>
      <tfoot>
      <tr>
        <th>Satuan ID</th>
        <th>Nama Satuan</th>
        <th>Action</th>
      </tr>
      </tfoot>
    </table>
  </div>
  <!-- /.box-body -->
</div>
@endsection
@section('js')
<script type="text/javascript">
$(function () {
  $('#example2').DataTable({
    'paging'      : true,
    'lengthChange': true,
    'searching'   : true,
    'ordering'    : true,
    'info'        : true,
    'autoWidth'   : false
  })
})
</script>
@endsection
