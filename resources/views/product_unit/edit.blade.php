@extends('layout.app')

@section('title', 'Edit Satuan')

@section('css')
@endsection

@section('content')
@if(session()->has('message'))
    <div class="alert alert-success">
        {{ session()->get('message') }}
    </div>
@endif
<div class="box box-primary">
  <div class="box-header with-border">
    <h3 class="box-title">Edit Satuan</h3>
  </div>
  <!-- /.box-header -->
  <!-- form start -->
  <form role="form" action="{{route('update_product_unit')}}" method="post">
    @csrf
    <div class="box-body">
      <div class="form-group">
        <label for="name">Nama Satuan</label>
        <input type="text" name="name" class="form-control" value="{{$product_unit->name}}" id="name" placeholder="Masukan Nama Satuan">
      </div>
      <input type="hidden" name="product_unit_id" value="{{$product_unit->id}}">
    </div>
    <!-- /.box-body -->

    <div class="box-footer">
      <button type="submit" class="btn btn-primary">Submit</button>
    </div>
  </form>
</div>
@endsection
