@extends('layout.app')

@section('title', 'List Product')

@section('css')
<!-- DataTables -->
<link rel="stylesheet" href="bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
@endsection
@section('content')
<div class="box">
  <div class="box-header">
    <h3 class="box-title">List Product</h3>
  </div>
  <!-- /.box-header -->
  <div class="box-body">
    <table id="example2" class="table table-bordered table-striped">
      <thead>
      <tr>
        <th>Product ID</th>
        <th>Brand</th>
        <th>Supplier</th>
        <th>Nama Product</th>
        <th>Satuan</th>
        <th>Harga</th>
        <th>Action</th>
      </tr>
      </thead>
      <tbody>
      @foreach($products as $product)
      <tr>
        <td>{{$product->id}}</td>
        <td>{{$product->brand->name}}</td>
        <td>{{$product->supplier->name}}</td>
        <td>{{$product->name}}</td>
        <td>{{$product->unit->name}}</td>
        <td>{{number_format($product->price,2,",",".")}}</td>
        <td><a href="{{route('edit_product')}}?product_id={{$product->id}}">Edit</a></td>
      </tr>
      @endforeach
      </tbody>
      <tfoot>
      <tr>
        <th>Product ID</th>
        <th>Brand</th>
        <th>Supplier</th>
        <th>Nama Product</th>
        <th>Satuan</th>
        <th>Harga</th>
        <th>Action</th>
      </tr>
      </tfoot>
    </table>
  </div>
  <!-- /.box-body -->
</div>
@endsection
@section('js')
<script type="text/javascript">
$(function () {
  $('#example2').DataTable({
    'paging'      : true,
    'lengthChange': true,
    'searching'   : true,
    'ordering'    : true,
    'info'        : true,
    'autoWidth'   : false
  })
})
</script>
@endsection
