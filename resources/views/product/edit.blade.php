@extends('layout.app')

@section('title', 'Tambah Product')

@section('css')
<link rel="stylesheet" href="{{asset('bower_components/select2/dist/css/select2.min.css')}}">
<style media="screen">
  .select2-container--default .select2-selection--single .select2-selection__rendered {
    line-height: 24px;
  }
  .select2-container .select2-selection--single .select2-selection__rendered {
    padding-left: 0;
  }
  .select2-container .select2-selection--single {
    height: auto;
  }
  .select2-container--default .select2-selection--single {
    border-radius: 0;
  }
</style>
@endsection

@section('content')
@if(session()->has('message'))
    <div class="alert alert-success">
        {{ session()->get('message') }}
    </div>
@endif
<div class="box box-primary">
  <div class="box-header with-border">
    <h3 class="box-title">Tambah Product</h3>
  </div>
  <!-- /.box-header -->
  <!-- form start -->
  <form role="form" action="{{route('update_product')}}" method="post">
    @csrf
    <div class="box-body">
      <div class="form-group">
        <label for="brand">Brand</label>
        <select id="brand" class="" name="brand_id" class="form-control mySelect" style="width: 100%;">
          @foreach($brands as $brand)
          <option value="{{$brand->id}}" {{($brand->id == $product->brand_id) ? 'selected' : ''}}>{{$brand->name}}</option>
          @endforeach
        </select>
      </div>
      <div class="form-group">
        <label for="supplier">Supplier</label>
        <select id="supplier" class="" name="supplier_id" class="form-control mySelect" style="width: 100%;">
          @foreach($suppliers as $supplier)
          <option value="{{$supplier->id}}" {{($supplier->id == $product->supplier_id) ? 'selected' : ''}}>{{$supplier->name}}</option>
          @endforeach
        </select>
      </div>
      <div class="form-group">
        <label for="name">Nama Product</label>
        <input class="form-control" id="name" type="text" name="name" value="{{$product->name}}" placeholder="Masukan Nama Product">
      </div>
      <div class="form-group">
        <label for="unit">Satuan</label>
        <select id="unit" class="" name="unit_id" class="form-control mySelect" style="width: 100%;">
          @foreach($units as $unit)
          <option value="{{$unit->id}}" {{($unit->id == $product->unit_id) ? 'selected' : ''}}>{{$unit->name}}</option>
          @endforeach
        </select>
      </div>
      <div class="form-group">
        <label for="price">Harga Jual</label>
        <input class="form-control" id="price" type="text" name="price" value="{{$product->price}}" placeholder="Masukan Harga">
      </div>
      <div class="form-group">
        <label for="price">Harga Modal</label>
        <input class="form-control" id="price_nett" type="text" name="price_nett" value="{{$product->price_nett}}" placeholder="Masukan Harga">
      </div>
      <div class="form-group">
        <label for="description">Deskripsi Product</label>
        <input class="form-control" id="description" type="text" name="description" value="{{$product->description}}" placeholder="Masukan Deskripsi Product">
      </div>
      <input type="hidden" name="product_id" value="{{$product->id}}">
    </div>
    <!-- /.box-body -->

    <div class="box-footer">
      <button type="submit" class="btn btn-primary">Submit</button>
    </div>
  </form>
</div>
@endsection

@section('js')
<script src="{{asset('bower_components/select2/dist/js/select2.full.min.js')}}"></script>
<script type="text/javascript">
$(document).ready(function () {
  //Initialize Select2 Elements
  $('select').select2();
});
</script>
@endsection
