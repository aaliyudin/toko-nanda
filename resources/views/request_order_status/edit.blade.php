@extends('layout.app')

@section('title', 'Edit Request Order Status')

@section('css')
@endsection

@section('content')
@if(session()->has('message'))
    <div class="alert alert-success">
        {{ session()->get('message') }}
    </div>
@endif
<div class="box box-primary">
  <div class="box-header with-border">
    <h3 class="box-title">Edit Request Order Status</h3>
  </div>
  <!-- /.box-header -->
  <!-- form start -->
  <form role="form" action="{{route('update_request_order_status')}}" method="post">
    @csrf
    <div class="box-body">
      <div class="form-group">
        <label for="name">Nama Request Order Status</label>
        <input type="text" name="name" class="form-control" value="{{$request_order_status->name}}" id="name" placeholder="Masukan Nama Request Order Status">
      </div>
      <input type="hidden" name="request_order_status_id" value="{{$request_order_status->id}}">
    </div>
    <!-- /.box-body -->

    <div class="box-footer">
      <button type="submit" class="btn btn-primary">Submit</button>
    </div>
  </form>
</div>
@endsection
