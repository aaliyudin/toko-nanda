
@extends('layout.app')

@section('title', 'List Pembeli')

@section('css')
<!-- DataTables -->
<link rel="stylesheet" href="bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
@endsection
@section('content')
<div class="box">
  <div class="box-header">
    <h3 class="box-title">List Pembeli</h3>
  </div>
  <!-- /.box-header -->
  <div class="box-body">
    <table id="example2" class="table table-bordered table-striped">
      <thead>
      <tr>
        <th>Pembeli ID</th>
        <th>Nama Pembeli</th>
        <th>Telp</th>
        <th>Alamat</th>
        <th>Company</th>
        <th>Action</th>
      </tr>
      </thead>
      <tbody>
      @foreach($buyers as $buyer)
      <tr>
        <td>{{$buyer->id}}</td>
        <td>{{$buyer->name}}</td>
        <td>{{$buyer->phone}}</td>
        <td>{{$buyer->address}}</td>
        <td>{{$buyer->company}}</td>
        <td><a href="{{route('edit_buyer')}}?buyer_id={{$buyer->id}}">Edit</a></td>
      </tr>
      @endforeach
      </tbody>
      <tfoot>
      <tr>
        <th>Pembeli ID</th>
        <th>Nama Pembeli</th>
        <th>Telp</th>
        <th>Alamat</th>
        <th>Company</th>
        <th>Action</th>
      </tr>
      </tfoot>
    </table>
  </div>
  <!-- /.box-body -->
</div>
@endsection
@section('js')
<script type="text/javascript">
$(function () {
  $('#example2').DataTable({
    'paging'      : true,
    'lengthChange': true,
    'searching'   : true,
    'ordering'    : true,
    'info'        : true,
    'autoWidth'   : false
  })
})
</script>
@endsection
