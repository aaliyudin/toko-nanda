@extends('layout.app')

@section('title', 'Edit Pembeli')

@section('css')
<link rel="stylesheet" href="{{asset('bower_components/select2/dist/css/select2.min.css')}}">
<style media="screen">
  .select2-container--default .select2-selection--single .select2-selection__rendered {
    line-height: 24px;
  }
  .select2-container .select2-selection--single .select2-selection__rendered {
    padding-left: 0;
  }
  .select2-container .select2-selection--single {
    height: auto;
  }
  .select2-container--default .select2-selection--single {
    border-radius: 0;
  }
</style>
@endsection

@section('content')
@if(session()->has('message'))
    <div class="alert alert-success">
        {{ session()->get('message') }}
    </div>
@endif
<div class="box box-primary">
  <div class="box-header with-border">
    <h3 class="box-title">Edit Pembeli</h3>
  </div>
  <!-- /.box-header -->
  <!-- form start -->
  <form role="form" action="{{route('update_buyer')}}" method="post">
    @csrf
    <div class="box-body">
      <div class="form-group">
        <label for="name">Nama Pembeli</label>
        <input required class="form-control" id="name" type="text" name="name" value="{{$buyer->name}}" placeholder="Masukan Nama Pembeli">
      </div>
      <div class="form-group">
        <label for="phone">Telp</label>
        <input required class="form-control" id="phone" type="text" name="phone" value="{{$buyer->phone}}" placeholder="Masukan Telp">
      </div>
      <div class="form-group">
        <label for="address">Alamat</label>
        <input required class="form-control" id="address" type="text" name="address" value="{{$buyer->address}}" placeholder="Masukan Alamat">
      </div>
      <div class="form-group">
        <label for="company">Company</label>
        <input required class="form-control" id="company" type="text" name="company" value="{{$buyer->company}}" placeholder="Masukan Company">
      </div>
      @foreach($discount_brand as $key => $discountBrand)
      <div class="box-diskon-1">
        <div class="form-group">
          <label for="diskon-brand-1">Diskon Brand 1</label>
          <select id="diskon-brand-1" class="" name="diskon-brand[{{$key}}][id]" class="form-control mySelect" style="width: 100%;">
            <option value="0">Select</option>
            @foreach($brands as $brand)
            <option value="{{$brand->id}}" {{($brand->id == $discountBrand->brand_id) ? 'selected' : ''}}>{{$brand->name}}</option>
            @endforeach
          </select>
        </div>
        <div class="row">
          <div class="col-md-3">
            <div class="form-group">
              <label for="diskon-brand-1-1">diskon 1</label>
              <input class="form-control" id="diskon-brand-1-1" type="number" name="diskon-brand[{{$key}}][disk][]" value="{{$discountBrand->discount_1}}" step="0.01" placeholder="%">
            </div>
          </div>
          <div class="col-md-3">
            <div class="form-group">
              <label for="diskon-brand-1-2">diskon 2</label>
              <input class="form-control" id="diskon-brand-1-2" type="number" name="diskon-brand[{{$key}}][disk][]" value="{{$discountBrand->discount_2}}" step="0.01" placeholder="%">
            </div>
          </div>
          <div class="col-md-3">
            <div class="form-group">
              <label for="diskon-brand-1-3">diskon 3</label>
              <input class="form-control" id="diskon-brand-1-3" type="number" name="diskon-brand[{{$key}}][disk][]" value="{{$discountBrand->discount_3}}" step="0.01" placeholder="%">
            </div>
          </div>
        </div>
      </div>
      @endforeach
      <input type="hidden" name="buyer_id" value="{{$buyer->id}}">
    </div>
    <!-- /.box-body -->

    <div class="box-footer">
      <button type="submit" class="btn btn-primary">Submit</button>
    </div>
  </form>
</div>
@endsection
@section('js')
<script src="{{asset('bower_components/select2/dist/js/select2.full.min.js')}}"></script>
<script type="text/javascript">
$(document).ready(function () {
  //Initialize Select2 Elements
  $('select').select2();
});
</script>
@endsection
