@extends('layout.app')

@section('title', 'Tambah Order Status')

@section('css')
@endsection

@section('content')
@if(session()->has('message'))
    <div class="alert alert-success">
        {{ session()->get('message') }}
    </div>
@endif
<div class="box box-primary">
  <div class="box-header with-border">
    <h3 class="box-title">Tambah Order Status</h3>
  </div>
  <!-- /.box-header -->
  <!-- form start -->
  <form role="form" action="{{route('create_order_status')}}" method="post">
    @csrf
    <div class="box-body">
      <div class="form-group">
        <label for="name">Nama Order Status</label>
        <input type="text" name="name" class="form-control" id="name" placeholder="Masukan Nama Order Status">
      </div>
    </div>
    <!-- /.box-body -->

    <div class="box-footer">
      <button type="submit" class="btn btn-primary">Submit</button>
    </div>
  </form>
</div>
@endsection
