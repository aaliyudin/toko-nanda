@extends('layout.app')

@section('title', 'Tambah Supplier')

@section('css')
@endsection

@section('content')
@if(session()->has('message'))
    <div class="alert alert-success">
        {{ session()->get('message') }}
    </div>
@endif
<div class="box box-primary">
  <div class="box-header with-border">
    <h3 class="box-title">Tambah Supplier</h3>
  </div>
  <!-- /.box-header -->
  <!-- form start -->
  <form role="form" action="{{route('create_supplier')}}" method="post">
    @csrf
    <div class="box-body">
      <div class="form-group">
        <label for="name">Nama Supplier</label>
        <input class="form-control" id="name" type="text" name="name" value="" placeholder="Masukan Nama Supplier">
      </div>
      <div class="form-group">
        <label for="phone">Telp</label>
        <input class="form-control" id="phone" type="text" name="phone" value="" placeholder="Masukan Telp">
      </div>
      <div class="form-group">
        <label for="address">Alamat</label>
        <input class="form-control" id="address" type="text" name="address" value="" placeholder="Masukan Alamat">
      </div>
      <div class="form-group">
        <label for="company">Company</label>
        <input class="form-control" id="company" type="text" name="company" value="" placeholder="Masukan Company">
      </div>
    </div>
    <!-- /.box-body -->

    <div class="box-footer">
      <button type="submit" class="btn btn-primary">Submit</button>
    </div>
  </form>
</div>
@endsection
