

@extends('layout.app')

@section('title', 'List Supplier')

@section('css')
<!-- DataTables -->
<link rel="stylesheet" href="bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
@endsection
@section('content')
<div class="box">
  <div class="box-header">
    <h3 class="box-title">List Supplier</h3>
  </div>
  <!-- /.box-header -->
  <div class="box-body">
    <table id="example2" class="table table-bordered table-striped">
      <thead>
      <tr>
        <th>Supplier ID</th>
        <th>Nama Supplier</th>
        <th>Telp</th>
        <th>Alamat</th>
        <th>Company</th>
        <th>Action</th>
      </tr>
      </thead>
      <tbody>
      @foreach($suppliers as $supplier)
      <tr>
        <td>{{$supplier->id}}</td>
        <td>{{$supplier->name}}</td>
        <td>{{$supplier->phone}}</td>
        <td>{{$supplier->address}}</td>
        <td>{{$supplier->company}}</td>
        <td><a href="{{route('edit_supplier')}}?supplier_id={{$supplier->id}}">Edit</a></td>
      </tr>
      @endforeach
      </tbody>
      <tfoot>
      <tr>
        <th>Supplier ID</th>
        <th>Nama Supplier</th>
        <th>Telp</th>
        <th>Alamat</th>
        <th>Company</th>
        <th>Action</th>
      </tr>
      </tfoot>
    </table>
  </div>
  <!-- /.box-body -->
</div>
@endsection
@section('js')
<script type="text/javascript">
$(function () {
  $('#example2').DataTable({
    'paging'      : true,
    'lengthChange': true,
    'searching'   : true,
    'ordering'    : true,
    'info'        : true,
    'autoWidth'   : false
  })
})
</script>
@endsection
