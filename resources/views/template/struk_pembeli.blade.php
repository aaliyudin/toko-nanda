<!DOCTYPE html>
<html lang="en" dir="ltr">
<head>
  <meta charset="utf-8">
  <title></title>
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <link href="/css/print.css" rel="stylesheet" media="print" type="text/css">
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="{{asset('bower_components/bootstrap/dist/css/bootstrap.min.css')}}">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="{{asset('bower_components/font-awesome/css/font-awesome.min.css')}}">
  <!-- Ionicons -->
  <link rel="stylesheet" href="{{asset('bower_components/Ionicons/css/ionicons.min.css')}}">
  <!-- jvectormap -->
  <link rel="stylesheet" href="{{asset('bower_components/jvectormap/jquery-jvectormap.css')}}">
  <!-- Theme style -->
  <link rel="stylesheet" href="{{asset('dist/css/AdminLTE.min.css')}}">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="{{asset('dist/css/skins/_all-skins.min.css')}}">
  <link rel="stylesheet" href="bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
  <style media="screen">
  * {
    color: #000 !important;
    font-size: 10px !important;
    font-family: calibri !important;
    font-weight: 800 !important;
  }
  .list-group {
    padding: 0 10px 0 10px;
  }
  .list-group-item {
    padding: 1px;
  }
  .table>tbody>tr>td, .table>tbody>tr>th, .table>tfoot>tr>td, .table>tfoot>tr>th, .table>thead>tr>td, .table>thead>tr>th {
    padding: 1px 5px;
    border-color: #000 !important;
  }
  </style>
  <style media="print">
  @media print {
    html, body {
      border: 1px solid white;
      height: 99%;
      page-break-after: avoid !important;
      page-break-before: avoid !important;
      font-family: calibri !important;
      font-size: 10px !important;
      font-weight: 800 !important;
      color: #000 !important;
    }
    .print-display-none,
    .print-display-none * {
      display: none !important;
    }
    .print-visibility-hide,
    .print-visibility-hide * {
      visibility: hidden !important;
    }
    .printme,
    .printme * {
      visibility: visible !important;
    }
    .printme {
      position: absolute;
      left: 0;
      top: 0;
    }

  }
  @page
    {
        size:  auto;   /* auto is the initial value */
        margin: 0mm;  /* this affects the margin in the printer settings */
    }

    html
    {
        background-color: #FFFFFF;
        margin: 0px 20px;  /* this affects the margin on the html before sending to printer */
    }
  body {
    font-size: 10px;
    color: #000 !important;
  }
  .table {
    margin-top: 10px;
  }
  .table>tbody>tr>td, .table>tbody>tr>th, .table>tfoot>tr>td, .table>tfoot>tr>th, .table>thead>tr>td, .table>thead>tr>th {
    padding: 1px 5px;
    border-color: !important;
  }
  .list-group {
    padding: 0;
  }
  .list-group-item {
    padding: 1px;
  }
  </style>
</head>
<body>
  <div style="margin-top:30px">
    <div class="pull-left">
      <span>Kepada Yth.</span>
      <br>
      <span>{{$request_order['buyer']}}</span>
      <br>
      <b>{{$request_order['inv_no']}}</b>
      <br>
    </div>
    <div class="pull-right">
      <span>Tanggal</span>
      <br>
      <span>{{date('d-m-Y', strtotime($request_order['created_at']))}}</span>
    </div>
    @foreach($request_order_details as $brand => $ro_detail)
    <table class="table table-bordered">
      <thead>
        <tr>
          <th style="width: 10px">#</th>
          <th>Nama Product</th>
          <th>Harga</th>
          <th style="width: 100px">Qty</th>
          <th style="width: 100px">Discount</th>
          <th style="text-align:right">Total</th>
        </tr>
      </thead>
      <tbody id="request_order_details_render">
        <?php $i = 1 ?>
        @foreach($ro_detail['product'] as $product)
        <tr>
          <td>{{$i}}</td>
          <td>{{$product['name']}}</td>
          <td>{{number_format($product['price'],2,",",".")}}</td>
          <td>{{$product['qty']}}</td>
          <td>{{$product['discount']}}%</td>
          <td style="text-align:right" >{{number_format($product['total'],2,",",".")}}</td>
        </tr>
        <?php $i++ ?>
        @endforeach
      </tbody>
      <tfoot>
        <tr>
          <th colspan="2">Total {{$brand}}</th>
          <th></th>
          <th></th>
          <th></th>
          <th style="text-align:right" id="render_total_request_order_details_render">{{number_format($ro_detail['total'],2,",",".")}}</th>
        </tr>

        <tr>
          <th></th>
          <th></th>
          <th></th>
          <th colspan="2">Tambahan disc {{$brand}} {{$ro_detail['discount_buyer']}}%</th>
          <th style="text-align:right" id="render_total_request_order_details_render">{{number_format($ro_detail['discount_buyer_val'],2,",",".")}}</th>
        </tr>
        <tr>
          <th></th>
          <th></th>
          <th></th>
          <th colspan="2">Jumlah {{$brand}}</th>
          <th style="text-align:right" id="render_total_request_order_details_render">{{number_format($ro_detail['total_after_disc_buyer'],2,",",".")}}</th>
        </tr>
      </tfoot>
    </table>
    @endforeach
    <!-- DataTables -->

    <ul class="list-group list-group-unbordered" id="render_discount_list">
      <li class="list-group-item"><span>Jumlah tagihan <b>{{$request_order->buyer}}</b></span><b class="pull-right">{{number_format($request_order->total_order_after_disc,2,",",".")}}</b></li>
    </ul>

  </div>

  <!-- jQuery 3 -->
  <script src="{{asset('bower_components/jquery/dist/jquery.min.js')}}"></script>
  <script type="text/javascript">
    $(window).ready(function() {
      window.print();
    });
  </script>
</body>
</html>
