<?php $i = 1; $total = 0;?>
@foreach($requestOrderDetails as $key => $requestOrderDetail)
<tr>
  <td>{{$i}}.</td>
  <td>{{$requestOrderDetail['name']}}<input type="hidden" name="product_id[]" value="{{$key}}"></td>
  <td>{{$requestOrderDetail['brand']}}</td>
  <td>{{number_format($requestOrderDetail['price'],2,",",".")}}</td>
  <td>{{$requestOrderDetail['qty']}}<input type="hidden" name="qty[]" value="{{$requestOrderDetail['qty']}}"></td>
  <td>{{number_format($requestOrderDetail['total'],2,",",".")}}<input type="hidden" name="total[]" value="{{$requestOrderDetail['total']}}"></td>
</tr>

<?php
$i++;
$total += $requestOrderDetail['total'];
?>
@endforeach
<input type="hidden" id="total_product_request_order" name="total_price" value="{{$total}}">

@foreach($requestOrderIds as $requestOrderId)
<input type="hidden" name="request_order_id[]" value="{{$requestOrderId}}">
@endforeach
<script type="text/javascript">
var TOTAL = $('#total_product_request_order').val();
$('#order_list').DataTable({
  'paging'      : true,
  'lengthChange': true,
  'searching'   : true,
  'ordering'    : true,
  'info'        : true,
  'autoWidth'   : false
});
</script>
