@extends('layout.app')

@section('title', 'List Permintaan')

@section('css')
<!-- DataTables -->
<link rel="stylesheet" href="bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
<style media="screen">
  .list-group {
    padding: 0 ;
  }
  .list-group-item {
    padding: 1px;
  }
  .table>tbody>tr>td, .table>tbody>tr>th, .table>tfoot>tr>td, .table>tfoot>tr>th, .table>thead>tr>td, .table>thead>tr>th {
    padding: 1px;
  }
</style>
@endsection
@section('content')
<div class="box">
  <div class="box-header">
    <h3 class="box-title">List Order</h3>
  </div>
  <!-- /.box-header -->
  <div class="box-body">
    <table id="example2" class="table table-bordered table-striped">
      <thead>
      <tr>
        <th>No</th>
        <th>Invoice No</th>
        <th>Pembeli</th>
        <th>Total Pembayaran</th>
        <th>Tanggal</th>
        <th>Action</th>
      </tr>
      </thead>
      <tbody><?php $i = 1 ?>
      @foreach($request_orders as $request_order)
      <tr>
        <td>{{$i}}</td>
        <td>{{$request_order->inv_no}}</td>
        <td>{{$request_order->buyer->name}}</td>
        <td>{{number_format($request_order->total_order,2,",",".")}}</td>
        <td>{{$request_order->created_at}}</td>
        <td>
          <a class="btn btn-success" href="{{route('edit_request_order')}}?request_order_id={{$request_order->id}}"><i class="fa fa-pencil"></i></a>
          <button type="button" class="btn btn-default modal-button" data-id="{{$request_order->id}}"><i class="fa fa-eye"></i></button>
          <a class="btn btn-danger" href="{{route('delete_request_order')}}?request_order_id={{$request_order->id}}"><i class="fa fa-trash"></i></a>
        </td>
      </tr>
      <?php $i++ ?>
      @endforeach
      </tbody>
      <tfoot>
      <tr>
        <th>No</th>
        <th>Invoice No</th>
        <th>Pembeli</th>
        <th>Total Pembayaran</th>
        <th>Tanggal</th>
        <th>Action</th>
      </tr>
      </tfoot>
    </table>
  </div>
  <!-- /.box-body -->
</div>
<div class="modal fade" id="modal-default">
  <div class="modal-dialog">
    <div id="request_order_details_render" class="modal-content">
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal -->
@endsection
@section('js')
<script type="text/javascript">
$(function () {
  $('#example2').DataTable({
    'paging'      : true,
    'lengthChange': true,
    'searching'   : true,
    'ordering'    : true,
    'info'        : true,
    'autoWidth'   : false
  });
});
$(document).ready(function(){
  $('.modal-button').on('click', function () {
    var request_order_id = $(this).attr('data-id');
    $.ajax({
      url: '{{route("show_request_order_detail")}}',
      type: 'POST',
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      },
      data: {
        'request_order_id' : request_order_id
      }
    })
    .done(function(result){
      console.log(result);
      $('#request_order_details_render').html(result);
      $("#modal-default").modal();
    });
  });

  $('#print_for_buyer').on('click', function(){
    var request_order_id = $('#print_id').val();
    var url = "{{route('prnit_struk_buyer')}}?request_order_id="+request_order_id+"";
    window.open(url, '_blank');
    window.focus();
  });
});
</script>
@endsection
