@extends('layout.app')

@section('title', 'Edit Permintaan Order')

@section('css')
<link rel="stylesheet" href="{{asset('bower_components/select2/dist/css/select2.min.css')}}">
<style media="screen">
  .select2-container--default .select2-selection--single .select2-selection__rendered {
    line-height: 24px;
  }
  .select2-container .select2-selection--single .select2-selection__rendered {
    padding-left: 0;
  }
  .select2-container .select2-selection--single {
    height: auto;
  }
  .select2-container--default .select2-selection--single {
    border-radius: 0;
  }
</style>
@endsection

@section('content')
@if(session()->has('message'))
    <div class="alert alert-success">
        {{ session()->get('message') }}
    </div>
@endif
<div class="box box-primary">
  <div class="box-header with-border">
    <h3 class="box-title">Edit Permintaan Order</h3>
  </div>
  <!-- /.box-header -->
  <!-- form start -->
  <form role="form" action="{{route('update_request_order')}}" method="post">
    @csrf
    <div class="box-body">
      <div class="form-group">
        <label for="buyer">Pembeli</label>
        <select id="buyer" class="" name="buyer_id" class="form-control mySelect" style="width: 100%;">
          <option value="0">Select</option>
          @foreach($buyers as $buyer)
          <option value="{{$buyer->id}}" {{($buyer->id == $request_order->buyer_id) ? 'selected' : ''}}>{{$buyer->name}}</option>
          @endforeach
        </select>
      </div>
      <div class="form-group">
        <label for="supplier">Supplier</label>
        <select id="supplier" class="" name="supplier_id" class="form-control mySelect" style="width: 100%;">
          <option value="0">Select</option>
          @foreach($suppliers as $supplier)
          <option value="{{$supplier->id}}" {{($supplier->id == $request_order->supplier_id) ? 'selected' : ''}}>{{$supplier->name}}</option>
          @endforeach
        </select>
      </div>
      <div id="product-wrapper">
        <?php $i = 0 ?>
        @foreach($request_order->detail as $request_order_detail)
        <div class="product-list init row">
          <div class="col-md-4">
            <div class="form-group">
              <label for="product">Product</label>
              <select name="product_id[]" class="form-control product product-init" data-id="{{$i}}" style="width: 100%;">
                <option value="0">Select</option>
                @foreach($products as $product)
                <option value="{{$product->id}}" {{($product->id == $request_order_detail->product_id) ? 'selected' : ''}}>{{$product->name}}</option>
                @endforeach
              </select>
            </div>
          </div>
          <div class="col-md-2">
            <div class="input-group">
              <label for="price">Harga Satuan</label>
              <input name="price[]" type="text" readonly class="form-control price price-{{$i}}" value="{{$request_order_detail->product->price}}" placeholder="">
            </div>
          </div>
          <div class="col-md-1">
            <div class="input-group">
              <label for="qty">Qty</label>
              <input name="qty[]" type="number" class="form-control qty qty-0" data-id="{{$i}}" value="{{$request_order_detail->qty}}" placeholder="">
            </div>
          </div>
          <div class="col-md-1">
            <div class="input-group">
              <label for="discount_product">Diskon</label>
              <input name="discount_product[]" type="number" class="form-control discount_product discount_product-{{$i}}" data-id="{{$i}}" value="{{$request_order_detail->discount_product}}" placeholder="%">
            </div>
          </div>
          <div class="col-md-2">
            <div class="input-group">
              <label for="total_product_price">Total</label>
              <input name="total_product_price[]" type="text" readonly class="form-control total_product_price total_product_price-{{$i}}" value="{{$request_order_detail->total_price}}" placeholder="">
            </div>
          </div>
          @if($i == 0)
          <div class="col-md-2">
            <label for="">Action</label><br>
            <button type="button" name="add" class="btn btn-primary add_field_button"><i class="fa fa-plus"></i></button>
          </div>
          @else
          <div class="col-md-2">
            <label for="">Action</label><br>
            <button type="button" name="add" class="btn btn-danger remove_field"><i class="fa fa-times"></i></button>
          </div>
          @endif
        </div>
        <?php $i++ ?>
        @endforeach
      </div>
      <div class="row">
        <div class="col-md-3">
          <div class="form-group">
            <label for="total">Total</label>
            <input class="form-control" id="total" readonly type="number" name="total" value="" placeholder="">
          </div>
        </div>
        <div class="col-md-2">
          <label for="">Kalkulasi Total Product</label>
          <button type="button" name="button" class="btn btn-success" id="calculate-product-total">Kalkulasi</button>
        </div>
      </div>
      <div class="row">
        <div class="col-md-3">
          <div class="form-group">
            <label for="discount_order">Diskon Transaksi %</label>
            <input class="form-control" id="discount_order" type="number" name="discount_order" value="{{$request_order->discount_order}}" placeholder="%">
          </select>
        </div>
        </div>
      </div>
      <div class="row">
        <div class="col-md-3">
          <div class="form-group">
            <label for="discount_buyer">Diskon Pembeli %</label>
            <input class="form-control" id="discount_buyer" type="number" name="discount_buyer" value="{{$request_order->discount_buyer}}" placeholder="%">
          </select>
        </div>
        </div>
      </div>
      <div class="row">
        <div class="col-md-3">
          <div class="form-group">
            <label for="total_order">Total Transaksi</label>
            <input class="form-control" id="total_order" readonly type="number" name="total_order" value="{{$request_order->total_order}}" placeholder="">
          </select>
        </div>
        </div>
      </div>
      <button type="button" name="button" class="btn btn-success" id="calculate-order-total">Kalkulasi</button>
    </div>
    <input type="hidden" name="request_order_id" value="{{$request_order->id}}">
    <!-- /.box-body -->

    <div class="box-footer">
      <button type="submit" class="btn btn-primary">Submit</button>
    </div>
  </form>
</div>
@endsection

@section('js')
<script src="{{asset('bower_components/select2/dist/js/select2.full.min.js')}}"></script>
<script type="text/javascript">
$(document).ready(function () {
  //Initialize Select2 Elements
  $('select').select2();
  //rednering product by suppliers
  $('#supplier').on("select2:select", function(event) {
    var value = $(event.currentTarget).find("option:selected").val();
    console.log(value);
    $.ajax({
        url: '{{route("product_by_supplier")}}',
        type: 'POST',
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        data: {
          'supplier_id' : value
        }
    })
    .done(function(result){
      $('.product-init').empty();
      var def = '<option value="0">Select</option>';
      $('.product-init').append(def);
      var totalData = result.length;
      for (var i = 0; i < totalData; i++) {
        var html = '<option value="'+result[i].id+'">'+result[i].name+'</option>';
        $('.product-init').append(html);
      }

    });
  });

  var max_fields      = 10; //maximum input boxes allowed
	var wrapper   		= $("#product-wrapper"); //Fields wrapper
	var add_button      = $(".add_field_button"); //Add button ID

	var x = 0; //initlal text box count
	$(add_button).click(function(e){ //on add input button click
		e.preventDefault();
		if(x < max_fields){ //max input box allowed
      x++
      var html = '<div class="product-list row">';
      html +='<div class="col-md-4"><div class="form-group">';
      html +='<label for="product">Product</label>';
      html +='<select name="product_id[]" class="form-control product product-'+x+'" data-id="'+x+'" style="width: 100%;"><option value="0">Select</option></select>';
      html +='</div></div>';
      html +='<div class="col-md-2"><div class="input-group">';
      html +='<label for="price">Harga Satuan</label>';
      html +='<input name="price[]" type="text" readonly class="form-control price-'+x+'" placeholder="">';
      html +='</div></div>';
      html +='<div class="col-md-1"><div class="input-group">';
      html +='<label for="qty">Qty</label>';
      html +='<input name="qty[]" type="number" class="form-control qty qty-'+x+'" data-id="'+x+'" value="1"  placeholder="">';
      html +='</div></div>';
      html +='<div class="col-md-1"><div class="input-group">';
      html +='<label for="discount_product">Diskon</label>';
      html +='<input name="discount_product[]" type="number" class="form-control discount_product discount_product-'+x+'" data-id="'+x+'" value="0" placeholder="">';
      html +='</div></div>';
      html +='<div class="col-md-2"><div class="input-group">';
      html +='<label for="total_product_price">Diskon</label>';
      html +='<input name="total_product_price[]" type="text" readonly class="form-control total_product_price total_product_price-'+x+'" placeholder="">';
      html +='</div></div>';
      html +='<div class="col-md-2">';
      html +='<label for="">Action</label><br>';
      html +='<button type="button" name="add" class="btn btn-danger remove_field"><i class="fa fa-times"></i></button>';
      html +='</div>';
      html +='</div>';
			$(wrapper).append(html); //add input box
      $.ajax({
          url: '{{route("product_by_supplier")}}',
          type: 'POST',
          headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          },
          data: {
            'supplier_id' : $('#supplier').val()
          }
      })
      .done(function(result){
        $('.product-'+x+'').empty();
        var def = '<option value="0">Select</option>';
        $('.product-'+x+'').append(def);
        var totalData = result.length;
        for (var i = 0; i < totalData; i++) {
          var html = '<option value="'+result[i].id+'">'+result[i].name+'</option>';
          $('.product-'+x+'').append(html);
        }
        $('select').select2();
        getPrice();
        totalProductPriceByDisc();
        totalProductPriceByQty();
      });
		}
	});

	$(wrapper).on("click",".remove_field", function(e){ //user click on remove text
		e.preventDefault(); $(this).closest('.product-list').remove(); x--;
	})

  function getPrice(){
    $('.product').on("select2:select", function(event) {
      console.log('test');
      var value = $(event.currentTarget).find("option:selected").val();
      var id = $(this).attr("data-id");
      console.log(value);
      $.ajax({
        url: '{{route("product_get_price")}}',
        type: 'POST',
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        data: {
          'product_id' : value
        }
      })
      .done(function(result){
        console.log(result);
        console.log(id);
        $('.price-'+id).val(result.price);
        $('.total_product_price-'+id).val(result.price);
      });
    });
  }

  // $('.qty').on('keyup', function(){
  //   var id = $(this).attr('data-id');
  //   var price = $('.price-'+id).val();
  //   var qty = $(this).val();
  //   var discount = $('.discount_product-'+id).val();
  //
  //   var total = (price * qty *discount) / 100;
  //   console.log(price);
  //   console.log(qty);
  //   console.log(discount);
  //   console.log(total);
  // });

  function totalProductPriceByQty() {
    $('.qty').on('keyup change', function(){
      var id = $(this).attr('data-id');
      var price = $('.price-'+id).val();
      var qty = $(this).val();
      var discount = $('.discount_product-'+id).val();

      if (discount != 0) {
        var totalDiscount = (price * qty ) * discount / 100;
        var total = (price * qty) - totalDiscount;
      } else {
        var total = (price * qty);
      }
      console.log(total);
      console.log(totalDiscount);
      $('.total_product_price-'+id).val(total);
    });
  }

  function totalProductPriceByDisc() {
    $('.discount_product').on('keyup change', function(){
      var id = $(this).attr('data-id');
      var price = $('.price-'+id).val();
      var qty = $('.qty-'+id).val();
      var discount = $(this).val();

      if (discount != 0) {
        var totalDiscount = (price * qty ) * discount / 100;
        var total = (price * qty) - totalDiscount;
      } else {
        var total = (price * qty);
      }
      console.log(total);
      console.log(totalDiscount);
      $('.total_product_price-'+id).val(total);
    });
  }

  $('#calculate-product-total').on('click', function(){
    var totalData = $('.total_product_price').length;
    var product_total_price = 0;
    for (var i = 0; i < totalData; i++) {
      product_total_price += parseInt($('.total_product_price-'+i).val());
    }
    $('#total').val(product_total_price);
  });

  // $('#discount_order').on('keyup change', function(){
  //   var total = $('#total').val();
  //   var discount_order = $(this).val() * total / 100;
  //   var discount_buyer = $('#discount_buyer').val() * total / 100;
  //
  //   var total_order = total - discount_order - discount_buyer;
  //   console.log(total_order);
  //   $('#total_order').val(total_order);
  // });
  //
  // $('#discount_buyer').on('keyup change', function(){
  //   var total = $('#total').val();
  //   var discount_order = $('#discount_order').val() * total / 100;
  //   var discount_buyer = $(this).val() * total / 100;
  //
  //   var total_order = total - discount_order - discount_buyer;
  //   console.log(total_order);
  //   $('#total_order').val(total_order);
  // });

  $('#calculate-order-total').on('click', function(){
    var total = $('#total').val();
    if ($('#discount_order').val() != 0) {
      var discount_order = $('#discount_order').val() * total / 100;
      var total_order = total - discount_order;
      console.log(total_order);
      $('#total_order').val(total_order);
    } else {
      var total_order = total - 0;
      $('#total_order').val(total_order);
    }
    if ($('#discount_buyer').val() != 0) {
      var discount_buyer = $('#discount_buyer').val() * total_order / 100;
      var total_order_final = total_order - discount_buyer;
    } else {
      var total_order_final = total_order - 0;
      $('#total_order').val(total_order_final);
    }
    $('#total_order').val(total_order_final);
  });

  $('#calculate-product-total').trigger('click');
  totalProductPriceByDisc();
  totalProductPriceByQty();
  getPrice();

});
</script>
@endsection
