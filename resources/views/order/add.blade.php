@extends('layout.app')

@section('title', 'Buat Order')

@section('css')
<!-- DataTables -->
<link rel="stylesheet" href="{{asset('bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">
<link rel="stylesheet" href="{{asset('bower_components/select2/dist/css/select2.min.css')}}">
<style media="screen">
  .select2-container--default .select2-selection--single .select2-selection__rendered {
    line-height: 24px;
  }
  .select2-container .select2-selection--single .select2-selection__rendered {
    padding-left: 0;
  }
  .select2-container .select2-selection--single {
    height: auto;
  }
  .select2-container--default .select2-selection--single {
    border-radius: 0;
  }
  #request_order_list_wrapper {
    padding: 10px;
  }
</style>
@endsection

@section('content')
@if(session()->has('message'))
    <div class="alert alert-success">
        {{ session()->get('message') }}
    </div>
@endif
<div class="box box-primary">
  <div class="box-header with-border">
    <h3 class="box-title">Buat Order</h3>
  </div>
  <!-- /.box-header -->
  <!-- form start -->
  <form role="form" action="{{route('create_order')}}" method="post">
    @csrf
    <div class="box-body">
      <table id="request_order_list" class="table table-bordered table-striped">
        <thead>
        <tr>
          <th>#</th>
          <th>Invoice No</th>
          <th>Pembeli</th>
          <th>Total Pembayaran</th>
          <th>Tanggal</th>
        </tr>
        </thead>
        <tbody id="request_order_render">
          <form class="" action="index.html" method="post">
          @foreach($request_orders as $requestOrder)
          <tr>
            <td><input type="checkbox" name="checkbox_request_order[]" class="minimal-red" value="{{$requestOrder->id}}"></td>
            <td>{{$requestOrder->inv_no}}</td>
            <td>{{$requestOrder->buyer->name}}</td>
            <td>{{number_format($requestOrder->total_order,2,",",".")}}</td>
            <td>{{$requestOrder->created_at}}</td>
          </tr>
          @endforeach
        </form>
        </tbody>
        <tfoot>
        <tr>
          <th>#</th>
          <th>Invoice No</th>
          <th>Pembeli</th>
          <th>Total Pembayaran</th>
          <th>Tanggal</th>
        </tr>
        </tfoot>
      </table>
      <div class="box-footer">
        <label for="calculate-product-total">Proses Request Order</label><br>
        <button type="button" name="button" class="btn btn-success" id="calculate-product-total">Proses</button>
      </div>
      <table class="table table-bordered" id="order_list">
        <thead>
          <tr>
            <th style="width: 10px">#</th>
            <th>Nama Product</th>
            <th>Brand</th>
            <th>Harga</th>
            <th style="width: 100px">Qty</th>
            <th>Total</th>
          </tr>
        </thead>
        <tbody id="product_request_order_render">
        </tbody>
        <tfoot>
          <tr>
            <th>Total</th>
            <th></th>
            <th></th>
            <th></th>
            <th></th>
            <th id="render_total_product_request_order">Total</th>
          </tr>
        </tfoot>
      </table>
      <div class="box-footer">
        <button type="submit" class="btn btn-primary">Submit</button>
      </div>
    </div>
  </form>
</div>
@endsection

@section('js')
<script src="{{asset('bower_components/select2/dist/js/select2.full.min.js')}}"></script>
<script type="text/javascript">
$(document).ready(function () {
  //Initialize Select2 Elements
  $('select').select2();
  $('#request_order_list').DataTable({
    'paging'      : true,
    'lengthChange': true,
    'searching'   : true,
    'ordering'    : true,
    'info'        : true,
    'autoWidth'   : false
  });
  $('#supplier').on("select2:select", function(event) {
    var value = $(event.currentTarget).find("option:selected").val();
    console.log(value);
    $.ajax({
      url: '{{route("request_order_by_supplier")}}',
      type: 'POST',
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      },
      data: {
        'supplier_id' : value
      }
    })
    .done(function(result){
      console.log(result);
      $("#request_order_list").dataTable().fnDestroy();
      $('#request_order_render').html(result);
      $('#request_order_list').DataTable({
        'paging'      : true,
        'lengthChange': true,
        'searching'   : true,
        'ordering'    : true,
        'info'        : true,
        'autoWidth'   : false
      })
    });
  });

  $('#calculate-product-total').on('click', function(){
    var request_order_id = [];
    var i = 0;
    $('input[name=checkbox_request_order\\[\\]]').each(function() {
      if (this.checked) {
        request_order_id[i] = $(this).val();
      }
      i++;
    });
    console.log(request_order_id);
    $.ajax({
      url: '{{route("get_product_total_by_request_order")}}',
      type: 'POST',
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      },
      data: {
        'request_order_id' : request_order_id
      }
    })
    .done(function(result){
      console.log(result);
      $('#product_request_order_render').html(result);
      $('#render_total_product_request_order').html(TOTAL);
    });
  });

});
</script>
@endsection
