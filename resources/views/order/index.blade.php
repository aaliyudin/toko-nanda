@extends('layout.app')

@section('title', 'List Order')

@section('css')
<!-- DataTables -->
<link rel="stylesheet" href="bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
<style media="screen">
.list-group {
  padding: 0 10px 0 10px;
}
.list-group-item {
  padding: 1px;
}
</style>
@endsection
@section('content')
<div class="box">
  <div class="box-header">
    <h3 class="box-title">List Order</h3>
  </div>
  <!-- /.box-header -->
  <div class="box-body">
    <table id="example2" class="table table-bordered table-striped">
      <thead>
      <tr>
        <th>Order ID</th>
        <th>Invoice No</th>
        <th>Supplier</th>
        <th>Total Pembayaran</th>
        <th>Action</th>
      </tr>
      </thead>
      <tbody>
      @foreach($orders as $order)
      <tr>
        <td>{{$order->id}}</td>
        <td>{{$order->inv_no}}</td>
        <td>{{$order->supplier->name}}</td>
        <td>{{number_format($order->total_price,2,",",".")}}</td>
        <td>
          <input type="hidden" name="" value="{{$order->id}}">
          <button type="button" class="btn btn-default modal-button" data-id="{{$order->id}}">
            Lihat
          </button>
        </td>
      </tr>
      @endforeach
      </tbody>
      <tfoot>
      <tr>
        <th>Order ID</th>
        <th>Invoice No</th>
        <th>Supplier</th>
        <th>Total Pembayaran</th>
        <th>Action</th>
      </tr>
      </tfoot>
    </table>
  </div>
  <!-- /.box-body -->
</div>

<div class="modal fade" id="modal-default">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title"></h4>
      </div>
      <div class="modal-body">
        <table class="table table-bordered">
          <thead>
            <tr>
              <th style="width: 10px">#</th>
              <th>Nama Product</th>
              <th>Brand</th>
              <th>Harga</th>
              <th style="width: 100px">Qty</th>
              <th>Total</th>
            </tr>
          </thead>
          <tbody id="order_details_render">

          </tbody>
          <tfoot>
            <tr>
              <th>Total</th>
              <th></th>
              <th></th>
              <th></th>
              <th></th>
              <th id="render_total_order_details_render">Total</th>
            </tr>
          </tfoot>
        </table>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Tutup</button>
        <button type="button" id="print_for_buyer" class="btn btn-primary">Print Struk</button>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal -->
@endsection
@section('js')
<script type="text/javascript">
$(function () {
  $('#example2').DataTable({
    'paging'      : true,
    'lengthChange': true,
    'searching'   : true,
    'ordering'    : true,
    'info'        : true,
    'autoWidth'   : false
  })
})
$(document).ready(function(){
  $('.modal-button').on('click', function () {
    var order_id = $(this).attr('data-id');
    $.ajax({
      url: '{{route("show_order_detail")}}',
      type: 'POST',
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      },
      data: {
        'order_id' : order_id
      }
    })
    .done(function(result){
      console.log(result);
      $('#order_details_render').html('');
      $('.modal-title').html(result['order'].inv_no + " - " + result['order'].supplier.name);
      var orderDetailLength = result['order_details'].length;
      var no = 1;
      for (var i = 0; i < orderDetailLength; i++) {
        var table = "<tr>";
        table += "<td>"+no+"</td>";
        table += "<td>"+result['order_details'][i].name+"</td>";
        table += "<td>"+result['order_details'][i].brand+"</td>";
        table += "<td>"+result['order_details'][i].price.toFixed(2)+"</td>";
        table += "<td>"+result['order_details'][i].qty+"</td>";
        table += "<td>"+result['order_details'][i].total.toFixed(2)+"</td>";
        table += "</tr>";
        $('#order_details_render').append(table);
        no ++;
      }
      $('#render_total_order_details_render').html(result['order'].total_price);
      $("#modal-default").modal();
    });
  });
});
</script>
@endsection
