
<aside class="main-sidebar">
  <!-- sidebar: style can be found in sidebar.less -->
  <section class="sidebar">
    <!-- Sidebar user panel -->
    <div class="user-panel">
      <div class="pull-left image">
        <img src="{{asset('dist/img/user2-160x160.jpg')}}" class="img-circle" alt="User Image">
      </div>
      <div class="pull-left info">
        <p>PT Naget Harapan Terang</p>
        <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
      </div>
    </div>
    <!-- sidebar menu: : style can be found in sidebar.less -->
    <ul class="sidebar-menu" data-widget="tree">
      <li class="header">Data</li>
      <li class=" treeview ">
        <a href="#">
          <i class="fa fa-dashboard"></i> <span>Supplier</span>
          <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
          </span>
        </a>
        <ul class="treeview-menu">
          <li><a href="{{route('index_supplier')}}"><i class="fa fa-circle-o"></i> List Supplier</a></li>
          <li class=""><a href="{{route('add_supplier')}}"><i class="fa fa-circle-o"></i> Tambah Supplier</a></li>
        </ul>
      </li>
      <li class=" treeview ">
        <a href="#">
          <i class="fa fa-dashboard"></i> <span>Pembeli</span>
          <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
          </span>
        </a>
        <ul class="treeview-menu">
          <li><a href="{{route('index_buyer')}}"><i class="fa fa-circle-o"></i> List Pembeli</a></li>
          <li class=""><a href="{{route('add_buyer')}}"><i class="fa fa-circle-o"></i> Tambah Pembeli</a></li>
        </ul>
      </li>
      <li class=" treeview ">
        <a href="#">
          <i class="fa fa-dashboard"></i> <span>Brand</span>
          <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
          </span>
        </a>
        <ul class="treeview-menu">
          <li><a href="{{route('index_brand')}}"><i class="fa fa-circle-o"></i> List Brand</a></li>
          <li class=""><a href="{{route('add_brand')}}"><i class="fa fa-circle-o"></i> Tambah Brand</a></li>
        </ul>
      </li>
      <li class=" treeview ">
        <a href="#">
          <i class="fa fa-dashboard"></i> <span>Satuan Product</span>
          <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
          </span>
        </a>
        <ul class="treeview-menu">
          <li><a href="{{route('index_product_unit')}}"><i class="fa fa-circle-o"></i> List Satuan</a></li>
          <li class=""><a href="{{route('add_product_unit')}}"><i class="fa fa-circle-o"></i> Tambah Satuan</a></li>
        </ul>
      </li>
      <li class=" treeview hidden">
        <a href="#">
          <i class="fa fa-dashboard"></i> <span>Order Status</span>
          <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
          </span>
        </a>
        <ul class="treeview-menu">
          <li><a href="{{route('index_order_status')}}"><i class="fa fa-circle-o"></i> List Order Status</a></li>
          <li class=""><a href="{{route('add_order_status')}}"><i class="fa fa-circle-o"></i> Tambah Order Status</a></li>
        </ul>
      </li>
      <li class=" treeview hidden">
        <a href="#">
          <i class="fa fa-dashboard"></i> <span>Request Order Status</span>
          <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
          </span>
        </a>
        <ul class="treeview-menu">
          <li><a href="{{route('index_request_order_status')}}"><i class="fa fa-circle-o"></i> List Request Order Status</a></li>
          <li class=""><a href="{{route('add_request_order_status')}}"><i class="fa fa-circle-o"></i> Tambah Request Order Status</a></li>
        </ul>
      </li>
      <li class=" treeview ">
        <a href="#">
          <i class="fa fa-dashboard"></i> <span>Product</span>
          <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
          </span>
        </a>
        <ul class="treeview-menu">
          <li><a href="{{route('index_product')}}"><i class="fa fa-circle-o"></i>List Product</a></li>
          <li class=""><a href="{{route('add_product')}}"><i class="fa fa-circle-o"></i> Tambah Product</a></li>
        </ul>
      </li>
      <li class="header">Transaksi</li>
      <li class=" treeview ">
        <a href="#">
          <i class="fa fa-dashboard"></i> <span>Permintaan Order</span>
          <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
          </span>
        </a>
        <ul class="treeview-menu">
          <li><a href="{{route('index_request_order')}}"><i class="fa fa-circle-o"></i> List Permintaan Order</a></li>
          <li class=""><a href="{{route('add_request_order')}}"><i class="fa fa-circle-o"></i> Buat Permintaan Order</a></li>
        </ul>
      </li>
      <li class=" treeview ">
        <a href="#">
          <i class="fa fa-dashboard"></i> <span>Order</span>
          <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
          </span>
        </a>
        <ul class="treeview-menu">
          <li><a href="{{route('index_order')}}"><i class="fa fa-circle-o"></i>List Order</a></li>
          <li class=""><a href="{{route('add_order')}}"><i class="fa fa-circle-o"></i>Buat Order</a></li>
        </ul>
      </li>
    </ul>
  </section>
  <!-- /.sidebar -->
</aside>
